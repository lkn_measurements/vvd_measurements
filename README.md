# VVD: Dataset and Code Detailed Explanation
September 23, 2019

Updated: November 13, 2019

## Scenario and General Information

* The dataset is created for the **proof-of-concept of using depth images in the improvement of wireless communication**. One receiver, one transmitter and one mobile human was involved in all the measurements.
* This dataset is obtained in indoor environment at TU Munich with off-the-shelf sensor (Zolertia RE-Mote) transmissions within 802.15.4 standard which are received and stored by an SDR (USRP X310). Sampling rate was 8 MHz. 
* Depth Images in the dataset are obtained by Stereolabs ZED RGB-D camera which was operated
at 30 fps at 720p video mode during the measurements.
* One of the videos (*.svo file) recorded during the measurements is uploaded to provide an example (Only to [Cloud repository](https://cloud.lkn.ei.tum.de/s/WGLAWLowdkoatT3) due to the size). Note that ZED SDK is required in order to watch the video.
* More details can be found on the original paper named as "Veni Vidi Dixi: Reliable Wireless Communication with Depth Images"
* Please note that in order to run the codes, Matlab active directory should be the **Artifacts_Evaluation** folder and when Matlab Editor warning dialog appears for not found code, **Add to path** should be selected. In case of any other directory error, check which part is causing the error and put these **folders** into the same folder with the code.
* For all of the dataset and results, Matlab 2018a, TensorFlow with GPU Support in Jupyter notebook and Python 3.6 is utilized. A nice guide for installing TensorFlow with GPU support is in this [link](https://www.pugetsystems.com/labs/hpc/The-Best-Way-to-Install-TensorFlow-with-GPU-Support-on-Windows-10-Without-Installing-CUDA-1187/).

## Provided Data

* **Received wireless waveforms** during the measurements in "Raw\_datasets" for each measurement set. These include also channel estimations and serve as main dataset.
* **Channel estimations** obtained with LS estimation in "Obtained\_Ground\_Chan\_Ests" folder for each measurement set. These provides flexibility to the user if the user does not require IQ samples. 
* Note that in the ".mat" file there exist 4 channel estimations: LS\_11Tap, LS\_11Tap\_Corr, Preamble\_LS11Tap, and Preamble\_LS11Tap_Corr. First two are the estimations that are obtained from the whole received signal, whereas the latter 2 are the estimations obtained from the preamble part of the signal. The suffix "\_Corr" are the mean phase shifted versions of the same estimations to make estimations relatable in Machine Learning.
* **Channel estimation predictions obtained with VVD and Kalman** filtering based channel estimation are provided in "Chan\_Est\_Datasets/" folder.
* **Depth image frames** of the recorded video during the measurements in "Images/Set[Num]" folders for each measurement set within their respective measurement number. The given ".npy" files are the depth image frames concatenated along row axis. There exist 2 versions: one with subsampling factor of 10 and one with subsampling factor of 4. Although, subsampling factor of 10 data is used in the VVD paper, the other is shared for further research.
* **Codes** are shared in "Codes" folder. The codes use the given datasets and produces the results provided in the paper.
* The **relation between image frame number and received packets** can be found inside the "Frame\_Number\_Creation" folder. It explains how the frame numbers are added to the datasets since the frame matching is done in a different manner.
* The **ML training results and model weights for VVD** for 3 variants are provided in "Codes/Keras\_ML\_Model/" folders. Each variant has its own prediction and ML weight result for each set combination. Set combination info is also provided as text file in the folder. Each set combination result includes the prediction results for the VVD algorithm, the loss graphs, the saved CNN model and CNN weights as well.

## How to Create Dataset from Captured Data

* **"Dataset\_Raw\_Data\_2\_Chan\_Est.m"** code within "Codes" folder should be run. This code uses the stored wireless IQ samples within "Raw\_Datasets" folder and saves channel estimations into "Chan\_Est\_Datesets" folder. Relative paths to files are given in the beginning of the code, hence change the matlab folder or change the paths according to your PC configuration. Note that these estimations are already given in "Obtained\_Ground\_Chan\_Est\_Datasets" folder.
* First 6 Cells of **"VVD\_Scripts.ipynb"** code should be run. Cell number 5 \& 6 (Excluding the markdown cells) creates the ".npy" version of the channel estimations for ease of usage for each measurement set. These cells create the depth images from the image collection in "Images" folder to ".npy" data into "Set\_by\_set" folder to be inputs of Machine Learning for each measurement set. Here, the subsampling factor in Cell 3 can be changed to 4 instead of 10 to work on bigger resolution.

## How to Visualize the Data

* **Images** can be visualized by using the "npy\_image\_extraction.py". The aim of this code is to provide flexibility to the user to observe image frames of the dataset outside of jupyter notebook. However, in jupyter-notebook it is also easy to visualize images which will be explained in next section.
* The **channel estimations** can be visualized as in the "Plot\_Fig5.m" code in "Codes" folder. This code both visualizes the Figure 5 (Hypothesis testing figure) in the VVD original paper, and provides an example how one could extract data from the given dataset ".mat" files.

## How to VVD in Jupyter Notebook

* **"VVD\_Scripts.ipynb"** code should be used to produce VVD algorithm results. Cell 3 contains the parameters such as what subsampling factor is used, which measurement sets are in training, validation and test sets. Note that tensorflow with GPU support is required for this code to run without error.
* After "Estimation concatenations (Training, Validation and Test set preparation)" title in the ".ipynb" the Machine Learning part begins. Next 5 cells concatenate the channel estimations and **create Training, Validation and Test sets for the output of the Machine Learning model**. (Note that 2 cells are for visualization of the channel estimations in the notebook!)
* After "Image Data Concatenation (Training, Validation and Test set preparation)" title in the ".ipynb", next 3 cells concatenate the depth images such as the **Training, Validation and Test sets to create the input of the Machine Learning model**. Note that this part uses the "_Corr" version of the stored estimations!
* After "Further subsampling on loaded frames (Next 3 Cells)" title 3 cells can be used to further **increase the subsampling** after loading the images. (**Optional and not used in VVD paper**)
* After "**Visualization of Depth Images in Jupyter Notebook**" the next cell is to provide visualization in Jupyter notebook for the depth images. This allows one to estimate which parts of the image should be cropped without harming the results.
* After the title "**Cropping for image size reduction** (as in VVD Paper)", 3 cells are cropping the created training, validation and test sets of the image frames to further reduce input size. Cropping is done in the original paper due to GPU memory limitations such that via cropping larger batch size could be used. Cropping is done on the parts where mobile human does not visit during all measurement sets.
* After "**Our Proposed CNN Model for VVD**" title, next cell builds the proposed CNN model.
* After "Callbacks for including adaptive learning rate, checkpoint", next cell provides the **ML callbacks** such as ML weights are saved after each better validation set performing epoch, and enabling learning rate decay. 
* Next 5 Cells **finalizes the input and output sets** by normalizing and shaping them to make them fit into the model. Normalization factor of the training set channel estimation is found and applied on all sets.
* After "ML model training as used in the Paper" title, next cell **starts the training of CNN algorithm** that is used.
* Next 3 cells are to **store the loss variation during training** which can already be seen in "Codes/Keras\_ML\_models/" folder.
* Next cell after the title "Rest is to obtain and visualize predictions for the test set with the trained ML model" is to **obtain predictions for channel estimations from the trained ML model (VVD)**. The output of this cell provides information regarding run time of the CNN model, and visualization of the prediction.
* Last two cells **reshapes the predictions** obtained via VVD and **stores them into .mat files** for later usage which are already provided in "Chan\_Est\_Datasets/VVD\_Ests/" for all VVD variants.

## How to Obtain Results of VVD Paper

* Note that Communications Toolbox™ Library for the ZigBee® Protocol of MATLAB is required. 
* Run "**Main.m**" code in the "Codes" folder. This code loads all estimations for each received packet for comparison. Note that relative paths are given in code, hence change the matlab folder or change the paths according to your PC configuration. The code will **provide the results except the Aging and Performance Insight** results.
* Run the codes within the **"Aging\_Codes" folder** to obtain **Aging Insights results** that are shown in the original paper.
* Keep all the results in the variables section in matlab so that they can be used directly for plotting purposes altogether.

## How to Plot

* Run "**Plot\_Main\_Results.m**" code in the "Codes/Plot/" folder. This code provides **plots except the aging plots** in the paper. At the beginning there exists a load function. With this function you can plot the results without any prior code run. If you have visited the previous sections' codes you may need to comment out this line.
* Run "**Plots\_Aging\_TimevsPER.m**" code to **plot Aging and Performance Insights plots** in the paper. Again if you have run already the "Main.m" code you may not need the load functions at the beginning of the code.
* Run "**Plot\_Fig5.m**" code in order to plot the Figure 5 in the VVD paper which is **Hypothesis Testing figure**.
* Note that the colored images in **Figure 4 in the paper** are also provided in "Codes/Plot/" folder.

## Further Information

* The number of occupants in the lab was limited to one person during the measurements. The person has been informed and involved about the procedure and the purpose of the data collection.
* Raw IQ samples are stored by the help of GNU Radio software taking the readings from an SDR (USRP X310).
* Measurement videos are recorded with the help of ZED SDK using ZED Camera at 720p Video Mode and 30 fps.
* Communications, DSP System, Signal Processing Toolboxes and Communications Toolbox Library for the ZigBee Protocol is utilized within the provided codes. Deep Learning Toolbox is also used but only for the average computation time measurement.
