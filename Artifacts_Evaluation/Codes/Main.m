clear all;
close all;
clc;
%% Add folder paths
addpath('./Obtained_Ground_Chan_Est_Datasets/');
addpath('./Raw_Datasets/');
addpath('./Chan_Est_Datasets/Kalman_Ests/');
addpath('./Chan_Est_Datasets/VVD_Ests/');
addpath('./Codes/');
addpath('./Codes/Helpers/');
%% Specifies which sets are to be used as train and set
% Below set should be exactly the same with the sets of Machine learning
% algorithm that is to be compared
sets(1).set_train = [1,2,3,4,5,7,9,10,11,12,13,14,15];
sets(1).set_validation = 6;
sets(1).set_test = 8;

sets(2).set_train = [1,2,3,4,5,6,7,8,9,10,12,13,14];
sets(2).set_validation = 11;
sets(2).set_test = 15;

sets(3).set_train = [1,2,3,4,5,6,7,8,10,11,12,13,15];
sets(3).set_validation = 14;
sets(3).set_test = 9;

sets(4).set_train = [1,3,4,6,7,8,9,10,11,12,13,14,15];
sets(4).set_validation = 5;
sets(4).set_test = 2;

sets(5).set_train = [1,2,3,5,6,7,8,9,10,11,13,14,15];
sets(5).set_validation = 12;
sets(5).set_test = 4;

sets(6).set_train = [2,3,4,5,6,7,8,9,11,12,13,14,15];
sets(6).set_validation = 10;
sets(6).set_test = 1;

sets(7).set_train = [1,2,3,4,5,7,8,10,11,12,13,14,15];
sets(7).set_validation = 9;
sets(7).set_test = 6;

sets(8).set_train = [1,2,4,5,6,7,8,9,10,11,12,14,15];
sets(8).set_validation = 13;
sets(8).set_test = 3;

sets(9).set_train = [1,2,3,4,6,7,9,10,11,12,13,14,15];
sets(9).set_validation = 8;
sets(9).set_test = 5;

sets(15).set_train = [1,2,3,5,6,8,9,10,11,12,13,14,15];
sets(15).set_validation = 4;
sets(15).set_test = 7;

sets(11).set_train = [1,2,4,5,6,7,8,9,11,12,13,14,15];
sets(11).set_validation = 3;
sets(11).set_test = 10;

sets(12).set_train = [1,2,3,4,5,6,8,9,10,12,13,14,15];
sets(12).set_validation = 7;
sets(12).set_test = 11;

sets(13).set_train = [1,2,3,4,5,6,7,8,9,10,11,14,15];
sets(13).set_validation = 13;
sets(13).set_test = 12;

sets(14).set_train = [1,3,4,5,6,7,8,9,10,11,12,14,15];
sets(14).set_validation = 2;
sets(14).set_test = 13;

sets(10).set_train = [2,3,4,5,6,7,8,9,10,11,12,13,15];
sets(10).set_validation = 1;
sets(10).set_test = 14;

version_len = 15;
for set_v=15:version_len
disp(['Set Version: ' num2str(set_v)]);

set_train = sets(set_v).set_train;
set_validation = sets(set_v).set_validation;
set_test = sets(set_v).set_test;

% Load predictions and raw signal
i = set_test; % Set number that is to be compared
only_prediction_file = strcat('./Obtained_Ground_Chan_Est_Datasets/Dataset_set',int2str(i),'_only_est_eq_phase.mat');
load(only_prediction_file);     % Loaded as 'Dataset_set'

% Use below code for automized calculation for different sets O.w.Loaded as i.e. 'Dataset_set8'
raw_signal_file = strcat('./Raw_Datasets/Dataset_set',int2str(i),'.mat');
raw_dataset = load(raw_signal_file);          
struct_str_dataset = fieldnames(raw_dataset);
struct_str_dataset=struct_str_dataset{1};
raw_dataset = raw_dataset.(struct_str_dataset);

load(strcat('./Chan_Est_Datasets/VVD_Ests/set',int2str(i),'_11Tap_ML_predict_3future.mat')) % Loaded as 'prediction_LS_11Tap'
prediction_LS_11Tap_3future = prediction_LS_11Tap;

load(strcat('./Chan_Est_Datasets/VVD_Ests/set',int2str(i),'_11Tap_ML_predict_1future.mat')) % Loaded as 'prediction_LS_11Tap'
prediction_LS_11Tap_1future = prediction_LS_11Tap;

load(strcat('./Chan_Est_Datasets/VVD_Ests/set',int2str(i),'_11Tap_ML_predict_current.mat')) % Loaded as 'prediction_LS_11Tap'
prediction_LS_11Tap_current = prediction_LS_11Tap;

load(strcat('./Chan_Est_Datasets/Kalman_Ests/set',int2str(i),'_Kalman_AR1.mat')) % Loaded as 'p_Kalman_AR1_all' & 'Normalization_factor '
load(strcat('./Chan_Est_Datasets/Kalman_Ests/set',int2str(i),'_Kalman_AR5.mat')) % Loaded as 'p_Kalman_AR5_all' & 'Normalization_factor '
load(strcat('./Chan_Est_Datasets/Kalman_Ests/set',int2str(i),'_Kalman_AR20.mat')) % Loaded as 'p_Kalman_AR20_all' & 'Normalization_factor '

% Parameters that are to be used
spc = 8;
decimationFactor = 1;
set_length = size(prediction_LS_11Tap_current,1);

% Clear variables if nmber of observed packets is to be changed

clear BitErr_NoEq_sum
clear BitErr_LS_sum
clear BitErr_Last_sum
clear BitErr_Last2_sum
clear BitErr_Last_Genie_sum
clear BitErr_Preamble_Genie_sum
clear BitErr_Preamble_sum
clear BitErr_Preamble_ML_sum
clear BitErr_Preamble_Kalman_AR20_sum
clear BitErr_ML_sum
clear BitErr_ML1_sum
clear BitErr_ML3_sum
clear BitErr_Kalman_AR1_Genie_sum
clear BitErr_Kalman_AR1_sum
clear BitErr_Kalman_AR5_sum
clear BitErr_Kalman_AR20_sum
% 
clear ChipErr_NoEq_sum
clear ChipErr_LS_sum
clear ChipErr_Last_sum
clear ChipErr_Last2_sum
clear ChipErr_Last_Genie_sum
clear ChipErr_Preamble_Genie_sum
clear ChipErr_Preamble_sum
clear ChipErr_Preamble_ML_sum
clear ChipErr_Preamble_Kalman_AR20_sum
clear ChipErr_ML_sum
clear ChipErr_ML1_sum
clear ChipErr_ML3_sum
clear ChipErr_Kalman_AR1_Genie_sum
clear ChipErr_Kalman_AR1_sum
clear ChipErr_Kalman_AR5_sum
clear ChipErr_Kalman_AR20_sum

clear PER_Count_No_Eq 
clear PER_Count_LS
clear PER_Count_Last
clear PER_Count_Last2
clear PER_Count_Last_Genie
clear PER_Count_Preamble_Genie
clear PER_Count_Preamble
clear PER_Count_Preamble_ML
clear PER_Count_Preamble_Kalman_AR20
clear PER_Count_ML
clear PER_Count_ML1
clear PER_Count_ML3
clear PER_Count_Kalman_AR1_Genie
clear PER_Count_Kalman_AR1
clear PER_Count_Kalman_AR5
clear PER_Count_Kalman_AR20

clear p_Preamble_4MSE
clear p_Preamble_ML_4MSE
clear p_Preamble_Kalman_AR20_4MSE
clear p_Preamble_Genie_4MSE
clear p_chanEst_4MSE
clear p_Last2_4MSE
clear p_Last_4MSE
clear p_Kalman_AR1_4MSE
clear Preamble_Found
% Packet Number specified to use same code after this part
% NOTE THAT IN THIS PART CODE SHOULD BE CHANGED FOR SPECIFIC
% DATASET!!!!!!!!!!!

% Take care of the initializations in Kalman Filter parts!! 
% (i.e. Dont take first 50 samples for comparison)
waveform_for_preamble = lrwpan.PHYGeneratorOQPSK(raw_dataset(1).MPDU, spc/decimationFactor, '2450 MHz'); % MPDU
preamble_waveform = waveform_for_preamble(1:1280,1);

disp('Start Comparison Calculations');

packet_num_range = 201:set_length; % This is to use meaningful part of the Kalman Filter based channel estimations. (To allow the algorithm to give results which are from steady state.)
for packet_num = packet_num_range
    disp(['Packet Num: ' num2str(packet_num) ' / ' num2str(packet_num_range(end))]);

    filteredOQPSK = raw_dataset(packet_num).Raw_Packet_Signal;
    Or_MPDU = raw_dataset(packet_num).MPDU;
    coarseFrequencyOffset = Dataset_set(packet_num).CFO;
    frame_num = Dataset_set(packet_num).Frame_Number;
    
    % Obtained estimations
    p_ML = prediction_LS_11Tap_current(packet_num,:).'; % Estimation from ML for current frame instant
    p_ML1 = prediction_LS_11Tap_1future(packet_num,:).'; % Estimation from ML for 1 future frame
    p_ML3 = prediction_LS_11Tap_3future(packet_num,:).'; % Estimation from ML for 3 future frame
    p_LS = Dataset_set(packet_num).LS_11Tap; % Original Estimation
    p_Last_Genie = Dataset_set(packet_num-1).LS_11Tap_Corr;
    
    % last estimation with intervals of 100ms
    if (packet_num-packet_num_range(1)+1)>5
        for last_ind=1:5
            if ~PER_Count_Last(packet_num-packet_num_range(1)+1-last_ind)
                break;
            end
        end
    else
        last_ind = 1;
    end
    p_Last = Dataset_set(packet_num-last_ind).LS_11Tap_Corr;
    p_Last_4MSE(:,packet_num) = Dataset_set(packet_num-last_ind).LS_11Tap_Corr;
    % last estimation with intervals of 500ms
    if (packet_num-packet_num_range(1)+1)>20
        for last_ind2=5:5:20
            if ~PER_Count_Last2(packet_num-packet_num_range(1)+1-last_ind2)
                break;
            end
        end
    else
        last_ind2 = 5;
    end
    p_Last2 = Dataset_set(packet_num-last_ind2).LS_11Tap_Corr;
    p_Last2_4MSE(:,packet_num) = Dataset_set(packet_num-last_ind2).LS_11Tap_Corr;
    
    p_Preamble = Dataset_set(packet_num).Preamble_LS_11Tap;
    p_Preamble_Genie = p_Preamble;
    p_Preamble_Genie_4MSE(:,packet_num) = Dataset_set(packet_num).Preamble_LS_11Tap_Corr;
    p_chanEst_4MSE(:,packet_num) = Dataset_set(packet_num).LS_11Tap_Corr;
    % Kalman adaptive to packet error
    if (packet_num-packet_num_range(1)+1)>5
        for last_ind_k=1:5
            if ~PER_Count_Kalman_AR1(packet_num-packet_num_range(1)+1-last_ind_k)
                break;
            end
        end
    else
        last_ind_k = 1;
    end
    p_Kalman_AR1 = p_Kalman_AR1_all(:,packet_num-last_ind_k+1);
    p_Kalman_AR1_4MSE(:,packet_num) = p_Kalman_AR1;
    p_Kalman_AR1_Genie = p_Kalman_AR1_all(:,packet_num);
    p_Kalman_AR5 = p_Kalman_AR5_all(:,packet_num);
    p_Kalman_AR20 = p_Kalman_AR20_all(:,packet_num);

    %% optional plotting part
%     figure(8);
%     hold on;plot(1:length(p_ML),abs(p_ML),'o');
%     % legend('LS channel estimate');
%     title('Absolute values of the impulse responses')
    
%     
%     figure(62);
%     hold on;
%     plot(real(p_LS2(:,1)),...
%         imag(p_LS2(:,1)),'o');
%     title('Filter Tap Coefficients in Constellation');
%     xlabel('In-Phase');
%     ylabel('Quadrature');
    
    % FIR zero forcing equalizer (LS approach)
    %% Frequency correction to the signal
    
    freqoffset = comm.PhaseFrequencyOffset('SampleRate',spc*1e6/decimationFactor,'FrequencyOffsetSource','Input port');
    coarseCompensatedOQPSK = freqoffset(filteredOQPSK,-coarseFrequencyOffset);
    
    %% Estimation Phase offsets are corrected here!

%     phase of p_LS,& p_Preamble are already correct as they are obtained from the received packet!

%     p_LS = Estimation_Phase_Corrector2(p_LS,preamble_waveform,...
%         coarseCompensatedOQPSK);
    
%     p_Preamble = Estimation_Phase_Corrector2(p_Preamble,preamble_waveform,...
%         coarseCompensatedOQPSK);

    p_Last_Genie = Estimation_Phase_Corrector2(p_Last_Genie,preamble_waveform,...
        coarseCompensatedOQPSK);
    
    p_Last = Estimation_Phase_Corrector2(p_Last,preamble_waveform,...
        coarseCompensatedOQPSK);
    
    p_Last2 = Estimation_Phase_Corrector2(p_Last2,preamble_waveform,...
        coarseCompensatedOQPSK);
    
    p_ML = Estimation_Phase_Corrector2(p_ML,preamble_waveform,...
        coarseCompensatedOQPSK);
    
    p_ML1 = Estimation_Phase_Corrector2(p_ML1,preamble_waveform,...
        coarseCompensatedOQPSK);
    
    p_ML3 = Estimation_Phase_Corrector2(p_ML3,preamble_waveform,...
        coarseCompensatedOQPSK);
    
    p_Kalman_AR1 = Estimation_Phase_Corrector2(p_Kalman_AR1,preamble_waveform,...
        coarseCompensatedOQPSK);
    
    p_Kalman_AR1_Genie = Estimation_Phase_Corrector2(p_Kalman_AR1_Genie,preamble_waveform,...
        coarseCompensatedOQPSK);
    
    p_Kalman_AR5 = Estimation_Phase_Corrector2(p_Kalman_AR5,preamble_waveform,...
        coarseCompensatedOQPSK);
    
    p_Kalman_AR20 = Estimation_Phase_Corrector2(p_Kalman_AR20,preamble_waveform,...
        coarseCompensatedOQPSK);
    %% Equalizers from corrected estimations are obtained
    c_LS = Estimation2Equalizer(p_LS);
    c_Last_Genie = Estimation2Equalizer(p_Last_Genie);
    c_Last = Estimation2Equalizer(p_Last);
    c_Last2 = Estimation2Equalizer(p_Last2);
    c_Kalman_AR1_Genie = Estimation2Equalizer(p_Kalman_AR1_Genie);
    c_Kalman_AR1 = Estimation2Equalizer(p_Kalman_AR1);
    c_Kalman_AR5 = Estimation2Equalizer(p_Kalman_AR5);
    c_Kalman_AR20 = Estimation2Equalizer(p_Kalman_AR20);
    c_ML = Estimation2Equalizer(p_ML);
    c_ML1 = Estimation2Equalizer(p_ML1);
    c_ML3 = Estimation2Equalizer(p_ML3);
    c_Preamble = Estimation2Equalizer(p_Preamble);
    
    %% Equalized Data acquisition
    No_Eq_data = coarseCompensatedOQPSK;
    LS_Eq_data = filter(c_LS,1,coarseCompensatedOQPSK); % Equalized data with LS Estimation (Ground Truth case)
    Last_Genie_Eq_data = filter(c_Last_Genie,1,coarseCompensatedOQPSK); % Equalized data with last packet's LS Estimation
    Last_Eq_data = filter(c_Last,1,coarseCompensatedOQPSK); % Equalized data with last packet's LS Estimation
    Last2_Eq_data = filter(c_Last2,1,coarseCompensatedOQPSK); % Equalized data with last packet's LS Estimation   
    Preamble_Eq_data = filter(c_Preamble,1,coarseCompensatedOQPSK); % Equalized data with LS Estimation from preamble
    ML_Eq_data = filter(c_ML,1,coarseCompensatedOQPSK); % Equalized data with Estimation from ML
    ML1_Eq_data = filter(c_ML1,1,coarseCompensatedOQPSK); % Equalized data with Estimation from ML
    ML3_Eq_data = filter(c_ML3,1,coarseCompensatedOQPSK); % Equalized data with Estimation from ML
    KAR1_Genie_Eq_data = filter(c_Kalman_AR1_Genie,1,coarseCompensatedOQPSK); % Equalized data with Estimation from Kalman AR1
    KAR1_Eq_data = filter(c_Kalman_AR1,1,coarseCompensatedOQPSK); % Equalized data with Estimation from Kalman AR1
    KAR5_Eq_data = filter(c_Kalman_AR5,1,coarseCompensatedOQPSK); % Equalized data with Estimation from Kalman AR5
    KAR20_Eq_data = filter(c_Kalman_AR20,1,coarseCompensatedOQPSK); % Equalized data with Estimation from Kalman AR20
    
%     scatterplot(Last_Eq_data);
%     scatterplot(Preamble_Eq_data);
    %% Decode signals obtained from different methods, Chip error rate is obtained here!!
        
    [MPDU_NoEq,phase_amb_NoEq,preamble_start_idx_NoEq, BitErr_NoEq, ChipErr_NoEq, PacketErr_NoEq, Preamble_found_flag] = PHYDecoderOQPSKNoSync_serkut_known_no_print_preamble(No_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_LS,phase_amb_LS,preamble_start_idx_LS, BitErr_LS, ChipErr_LS, PacketErr_LS] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(LS_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_Last_Genie,phase_amb_Last_Genie,preamble_start_idx_Last_Genie, BitErr_Last_Genie, ChipErr_Last_Genie, PacketErr_Last_Genie] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(Last_Genie_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_Last,phase_amb_Last,preamble_start_idx_Last, BitErr_Last, ChipErr_Last, PacketErr_Last] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(Last_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_Last2,phase_amb_Last2,preamble_start_idx_Last2, BitErr_Last2, ChipErr_Last2, PacketErr_Last2] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(Last2_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_Preamble_Genie,phase_amb_Preamble_Genie,preamble_start_idx_Preamble_Genie, BitErr_Preamble_Genie, ChipErr_Preamble_Genie, PacketErr_Preamble_Genie] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(Preamble_Eq_data,spc/decimationFactor,...
            Or_MPDU);
        
    Preamble_Found(packet_num-packet_num_range(1)+1) = Preamble_found_flag;
    if Preamble_found_flag
        [MPDU_Preamble,phase_amb_Preamble,preamble_start_idx_Preamble, BitErr_Preamble, ChipErr_Preamble, PacketErr_Preamble] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(Preamble_Eq_data,spc/decimationFactor,...
            Or_MPDU);
        
        MPDU_Preamble_ML = MPDU_Preamble;
        phase_amb_Preamble_ML = phase_amb_Preamble;
        preamble_start_idx_Preamble_ML = preamble_start_idx_Preamble;
        BitErr_Preamble_ML = BitErr_Preamble;
        ChipErr_Preamble_ML = ChipErr_Preamble;
        PacketErr_Preamble_ML = PacketErr_Preamble;
        
        MPDU_Preamble_Kalman_AR20 = MPDU_Preamble;
        phase_amb_Preamble_Kalman_AR20 = phase_amb_Preamble;
        preamble_start_idx_Preamble_Kalman_AR20 = preamble_start_idx_Preamble;
        BitErr_Preamble_Kalman_AR20 = BitErr_Preamble;
        ChipErr_Preamble_Kalman_AR20 = ChipErr_Preamble;
        PacketErr_Preamble_Kalman_AR20 = PacketErr_Preamble;

        p_Preamble_ML_4MSE(:,packet_num) = Dataset_set(packet_num).Preamble_LS_11Tap_Corr;
        p_Preamble_Kalman_AR20_4MSE(:,packet_num) = Dataset_set(packet_num).Preamble_LS_11Tap_Corr;
        
        p_Preamble_ML = p_Preamble;
        p_Preamble_Kalman_AR20 = p_Preamble;
    else
        MPDU_Preamble = MPDU_NoEq;
        phase_amb_Preamble = phase_amb_NoEq;
        preamble_start_idx_Preamble = preamble_start_idx_NoEq;
        BitErr_Preamble = BitErr_NoEq;
        ChipErr_Preamble = ChipErr_NoEq;
        PacketErr_Preamble = PacketErr_NoEq;
        
        p_Preamble = zeros(11,1);
        p_Preamble_ML = p_ML;
        p_Preamble_Kalman_AR20 = p_Kalman_AR20;
        
        p_Preamble_ML_4MSE(:,packet_num) = prediction_LS_11Tap_current(packet_num,:).';
        [MPDU_Preamble_ML,phase_amb_Preamble_ML,preamble_start_idx_Preamble_ML, BitErr_Preamble_ML, ChipErr_Preamble_ML, PacketErr_Preamble_ML] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(ML_Eq_data,spc/decimationFactor,...
            Or_MPDU);
        
        p_Preamble_Kalman_AR20_4MSE(:,packet_num) = p_Kalman_AR20_all(:,packet_num);
        [MPDU_Preamble_Kalman_AR20,phase_amb_Preamble_Kalman_AR20,preamble_start_idx_Preamble_Kalman_AR20, BitErr_Preamble_Kalman_AR20, ChipErr_Preamble_Kalman_AR20, PacketErr_Preamble_Kalman_AR20] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(KAR20_Eq_data,spc/decimationFactor,...
            Or_MPDU);
    end
    
    [MPDU_ML,phase_amb_ML,preamble_start_idx_ML, BitErr_ML, ChipErr_ML, PacketErr_ML] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(ML_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_ML1,phase_amb_ML1,preamble_start_idx_ML1, BitErr_ML1, ChipErr_ML1, PacketErr_ML1] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(ML1_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_ML3,phase_amb_ML3,preamble_start_idx_ML3, BitErr_ML3, ChipErr_ML3, PacketErr_ML3] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(ML3_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_Kalman_AR1_Genie,phase_amb_Kalman_AR1_Genie,preamble_start_idx_Kalman_AR1_Genie, BitErr_Kalman_AR1_Genie, ChipErr_Kalman_AR1_Genie, PacketErr_Kalman_AR1_Genie] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(KAR1_Genie_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_Kalman_AR1,phase_amb_Kalman_AR1,preamble_start_idx_Kalman_AR1, BitErr_Kalman_AR1, ChipErr_Kalman_AR1, PacketErr_Kalman_AR1] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(KAR1_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_Kalman_AR5,phase_amb_Kalman_AR5,preamble_start_idx_Kalman_AR5, BitErr_Kalman_AR5, ChipErr_Kalman_AR5, PacketErr_Kalman_AR5] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(KAR5_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    [MPDU_Kalman_AR20,phase_amb_Kalman_AR20,preamble_start_idx_Kalman_AR20, BitErr_Kalman_AR20, ChipErr_Kalman_AR20, PacketErr_Kalman_AR20] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(KAR20_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    
    %% Observed Bit Error Rates and Chip Error Rates are summed here to be averaged at the end!
    
    BitErr_NoEq_sum(packet_num-packet_num_range(1)+1) = BitErr_NoEq;
    ChipErr_NoEq_sum(packet_num-packet_num_range(1)+1) = ChipErr_NoEq;
    
    BitErr_LS_sum(packet_num-packet_num_range(1)+1) = BitErr_LS;
    ChipErr_LS_sum(packet_num-packet_num_range(1)+1) = ChipErr_LS;
    
    BitErr_Last_Genie_sum(packet_num-packet_num_range(1)+1) = BitErr_Last_Genie;
    ChipErr_Last_Genie_sum(packet_num-packet_num_range(1)+1) = ChipErr_Last_Genie;
    
    BitErr_Last_sum(packet_num-packet_num_range(1)+1) = BitErr_Last;
    ChipErr_Last_sum(packet_num-packet_num_range(1)+1) = ChipErr_Last;
    
    BitErr_Last2_sum(packet_num-packet_num_range(1)+1) = BitErr_Last2;
    ChipErr_Last2_sum(packet_num-packet_num_range(1)+1) = ChipErr_Last2;
    
    BitErr_Preamble_Genie_sum(packet_num-packet_num_range(1)+1) = BitErr_Preamble_Genie;
    ChipErr_Preamble_Genie_sum(packet_num-packet_num_range(1)+1) = ChipErr_Preamble_Genie;
    
    BitErr_Preamble_sum(packet_num-packet_num_range(1)+1) = BitErr_Preamble;
    ChipErr_Preamble_sum(packet_num-packet_num_range(1)+1) = ChipErr_Preamble;
    
    BitErr_Preamble_ML_sum(packet_num-packet_num_range(1)+1) = BitErr_Preamble_ML;
    ChipErr_Preamble_ML_sum(packet_num-packet_num_range(1)+1) = ChipErr_Preamble_ML;
    
    BitErr_Preamble_Kalman_AR20_sum(packet_num-packet_num_range(1)+1) = BitErr_Preamble_Kalman_AR20;
    ChipErr_Preamble_Kalman_AR20_sum(packet_num-packet_num_range(1)+1) = ChipErr_Preamble_Kalman_AR20;
    
    BitErr_ML_sum(packet_num-packet_num_range(1)+1) = BitErr_ML;
    ChipErr_ML_sum(packet_num-packet_num_range(1)+1) = ChipErr_ML;
    
    BitErr_ML1_sum(packet_num-packet_num_range(1)+1) = BitErr_ML1;
    ChipErr_ML1_sum(packet_num-packet_num_range(1)+1) = ChipErr_ML1;
    
    BitErr_ML3_sum(packet_num-packet_num_range(1)+1) = BitErr_ML3;
    ChipErr_ML3_sum(packet_num-packet_num_range(1)+1) = ChipErr_ML3;
    
    BitErr_Kalman_AR1_Genie_sum(packet_num-packet_num_range(1)+1) = BitErr_Kalman_AR1_Genie;
    ChipErr_Kalman_AR1_Genie_sum(packet_num-packet_num_range(1)+1) = ChipErr_Kalman_AR1_Genie;
    
    BitErr_Kalman_AR1_sum(packet_num-packet_num_range(1)+1) = BitErr_Kalman_AR1;
    ChipErr_Kalman_AR1_sum(packet_num-packet_num_range(1)+1) = ChipErr_Kalman_AR1;
    
    BitErr_Kalman_AR5_sum(packet_num-packet_num_range(1)+1) = BitErr_Kalman_AR5;
    ChipErr_Kalman_AR5_sum(packet_num-packet_num_range(1)+1) = ChipErr_Kalman_AR5;
    
    BitErr_Kalman_AR20_sum(packet_num-packet_num_range(1)+1) = BitErr_Kalman_AR20;
    ChipErr_Kalman_AR20_sum(packet_num-packet_num_range(1)+1) = ChipErr_Kalman_AR20;
    %% Packet Error Rate
    
    PER_Count_No_Eq(packet_num-packet_num_range(1)+1) = PacketErr_NoEq;
    PER_Count_LS(packet_num-packet_num_range(1)+1) = PacketErr_LS;
    PER_Count_Last_Genie(packet_num-packet_num_range(1)+1) = PacketErr_Last_Genie;
    PER_Count_Last(packet_num-packet_num_range(1)+1) = PacketErr_Last;
    PER_Count_Last2(packet_num-packet_num_range(1)+1) = PacketErr_Last2;
    PER_Count_Preamble_Genie(packet_num-packet_num_range(1)+1) = PacketErr_Preamble_Genie;
    PER_Count_Preamble(packet_num-packet_num_range(1)+1) = PacketErr_Preamble;
    PER_Count_Preamble_ML(packet_num-packet_num_range(1)+1) = PacketErr_Preamble_ML;
    PER_Count_Preamble_Kalman_AR20(packet_num-packet_num_range(1)+1) = PacketErr_Preamble_Kalman_AR20;
    PER_Count_ML(packet_num-packet_num_range(1)+1) = PacketErr_ML;
    PER_Count_ML1(packet_num-packet_num_range(1)+1) = PacketErr_ML1;
    PER_Count_ML3(packet_num-packet_num_range(1)+1) = PacketErr_ML3;
    PER_Count_Kalman_AR1_Genie(packet_num-packet_num_range(1)+1) = PacketErr_Kalman_AR1_Genie;
    PER_Count_Kalman_AR1(packet_num-packet_num_range(1)+1) = PacketErr_Kalman_AR1;
    PER_Count_Kalman_AR5(packet_num-packet_num_range(1)+1) = PacketErr_Kalman_AR5;
    PER_Count_Kalman_AR20(packet_num-packet_num_range(1)+1) = PacketErr_Kalman_AR20;
    
end
%% Getting an average over the obtained values part 1

PER_NoEq_mean(set_v) = mean(PER_Count_No_Eq);
PER_LS_mean(set_v) = mean(PER_Count_LS);
PER_Last_Genie_mean(set_v) = mean(PER_Count_Last_Genie);
PER_Last_mean(set_v) = mean(PER_Count_Last);
PER_Last2_mean(set_v) = mean(PER_Count_Last2);
PER_Preamble_Genie_mean(set_v) = mean(PER_Count_Preamble_Genie);
PER_Preamble_mean(set_v) = mean(PER_Count_Preamble);
PER_Preamble_mean_ML(set_v) = mean(PER_Count_Preamble_ML);
PER_Preamble_mean_Kalman_AR20(set_v) = mean(PER_Count_Preamble_Kalman_AR20);
PER_ML_mean(set_v) = mean(PER_Count_ML);
PER_ML1_mean(set_v) = mean(PER_Count_ML1);
PER_ML3_mean(set_v) = mean(PER_Count_ML3);
PER_Kalman_AR1_Genie_mean(set_v) = mean(PER_Count_Kalman_AR1_Genie);
PER_Kalman_AR1_mean(set_v) = mean(PER_Count_Kalman_AR1);
PER_Kalman_AR5_mean(set_v) = mean(PER_Count_Kalman_AR5);
PER_Kalman_AR20_mean(set_v) = mean(PER_Count_Kalman_AR20);

BER_NoEq_mean(set_v) = mean(BitErr_NoEq_sum);
BER_LS_mean(set_v) = mean(BitErr_LS_sum);
BER_Last_Genie_mean(set_v) = mean(BitErr_Last_Genie_sum);
BER_Last_mean(set_v) = mean(BitErr_Last_sum);
BER_Last2_mean(set_v) = mean(BitErr_Last2_sum);
BER_Preamble_Genie_mean(set_v) = mean(BitErr_Preamble_Genie_sum);
BER_Preamble_mean(set_v) = mean(BitErr_Preamble_sum);
BER_Preamble_mean_ML(set_v) = mean(BitErr_Preamble_ML_sum);
BER_Preamble_mean_Kalman_AR20(set_v) = mean(BitErr_Preamble_Kalman_AR20_sum);
BER_ML_mean(set_v) = mean(BitErr_ML_sum);
BER_ML1_mean(set_v) = mean(BitErr_ML1_sum);
BER_ML3_mean(set_v) = mean(BitErr_ML3_sum);
BER_Kalman_AR1_Genie_mean(set_v) = mean(BitErr_Kalman_AR1_Genie_sum);
BER_Kalman_AR1_mean(set_v) = mean(BitErr_Kalman_AR1_sum);
BER_Kalman_AR5_mean(set_v) = mean(BitErr_Kalman_AR5_sum);
BER_Kalman_AR20_mean(set_v) = mean(BitErr_Kalman_AR20_sum);

CER_NoEq_mean(set_v) = mean(ChipErr_NoEq_sum);
CER_LS_mean(set_v) = mean(ChipErr_LS_sum);
CER_Last_Genie_mean(set_v) = mean(ChipErr_Last_Genie_sum);
CER_Last_mean(set_v) = mean(ChipErr_Last_sum);
CER_Last2_mean(set_v) = mean(ChipErr_Last2_sum);
CER_Preamble_Genie_mean(set_v) = mean(ChipErr_Preamble_Genie_sum);
CER_Preamble_mean(set_v) = mean(ChipErr_Preamble_sum);
CER_Preamble_ML_mean(set_v) = mean(ChipErr_Preamble_ML_sum);
CER_Preamble_Kalman_AR20_mean(set_v) = mean(ChipErr_Preamble_Kalman_AR20_sum);
CER_ML_mean(set_v) = mean(ChipErr_ML_sum);
CER_ML1_mean(set_v) = mean(ChipErr_ML1_sum);
CER_ML3_mean(set_v) = mean(ChipErr_ML3_sum);
CER_Kalman_AR1_Genie_mean(set_v) = mean(ChipErr_Kalman_AR1_Genie_sum);
CER_Kalman_AR1_mean(set_v) = mean(ChipErr_Kalman_AR1_sum);
CER_Kalman_AR5_mean(set_v) = mean(ChipErr_Kalman_AR5_sum);
CER_Kalman_AR20_mean(set_v) = mean(ChipErr_Kalman_AR20_sum);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% For normalization purposes

% Chan_est_real_imag = [real(Chan_est);imag(Chan_est)];
% Normalization_factor = max(max(Chan_est_real_imag));

%  & 'Normalization_factor ' already obtained from
% load(strcat('set',int2str(i),'_Kalman_AR1.mat'))
% Loaded as 'p_Kalman_AR1_all' & 'Normalization_factor '
%% MSE comparison of the estimations
start = 201;
stop = size(p_chanEst_4MSE,2);
tap_s = size(p_chanEst_4MSE,1)*2; % *2 for real imag separation

Chan_est2_mse = [real(p_chanEst_4MSE.'),imag(p_chanEst_4MSE.')];
preamble_ML_mse = [real(p_Preamble_ML_4MSE.'),imag(p_Preamble_ML_4MSE.')];
preamble_Kalman_AR20_mse = [real(p_Preamble_Kalman_AR20_4MSE.'),imag(p_Preamble_Kalman_AR20_4MSE.')];
Chan_est_preamble_Genie_mse = [real(p_Preamble_Genie_4MSE.'),imag(p_Preamble_Genie_4MSE.')];
S_hat_mse1 = [real(p_Kalman_AR1_all.'),imag(p_Kalman_AR1_all.')];
S_hat_mse5 = [real(p_Kalman_AR5_all.'),imag(p_Kalman_AR5_all.')];
S_hat_mse20 = [real(p_Kalman_AR20_all.'),imag(p_Kalman_AR20_all.')];
ML_mse = [real(prediction_LS_11Tap_current),imag(prediction_LS_11Tap_current)];
ML1_mse = [real(prediction_LS_11Tap_1future),imag(prediction_LS_11Tap_1future)];
ML3_mse = [real(prediction_LS_11Tap_3future),imag(prediction_LS_11Tap_3future)];
Last2_mse = [real(p_Last2_4MSE.'),imag(p_Last2_4MSE.')];
Last_mse = [real(p_Last_4MSE.'),imag(p_Last_4MSE.')];
Kalman_AR1_mse = [real(p_Kalman_AR1_4MSE.'),imag(p_Kalman_AR1_4MSE.')];

MSE_AR1_Genie_taps(set_v) = sumabs((Chan_est2_mse(start:stop,:)-S_hat_mse1(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_AR1_taps(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Kalman_AR1_mse(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_AR5_taps(set_v) = sumabs((Chan_est2_mse(start:stop,:)-S_hat_mse5(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_AR20_taps(set_v) = sumabs((Chan_est2_mse(start:stop,:)-S_hat_mse20(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_ML(set_v) = sumabs((Chan_est2_mse(start:stop,:)-ML_mse(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_ML1(set_v) = sumabs((Chan_est2_mse(start:stop,:)-ML1_mse(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_ML3(set_v) = sumabs((Chan_est2_mse(start:stop,:)-ML3_mse(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_Preamble_ML(set_v) = sumabs((Chan_est2_mse(start:stop,:)-preamble_ML_mse(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_Preamble_Kalman_AR20(set_v) = sumabs((Chan_est2_mse(start:stop,:)-preamble_Kalman_AR20_mse(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_Preamble_Genie(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Chan_est_preamble_Genie_mse(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_Last_Genie(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Chan_est2_mse(start-1:stop-1,:)).^2)/((stop-start+1)*tap_s);
MSE_Last(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Last_mse(start:stop,:)).^2)/((stop-start+1)*tap_s);
MSE_Last2(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Last2_mse(start:stop,:)).^2)/((stop-start+1)*tap_s);

set_data_count(set_v) = size(packet_num_range,2);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Found Results to Visualize

notch = 'off';

%% MSE Results Visualization

figure;
boxplot([(MSE_ML3.'),(MSE_ML1.'),(MSE_ML.')],'Notch',notch,'Labels',{'VVD-100ms Future','VVD-33.3ms Future','VVD-Current'});%,'Whisker',10)
title(['MSE between the Versions' char(10) 'of Image Based Channel Estimation'])
grid on;
set(gca,'XTickLabelRotation',45,'FontSize',12)
ylabel('MSE')

figure;
boxplot([(MSE_AR1_taps.'),(MSE_AR5_taps.'),(MSE_AR20_taps.')],'Notch',notch,'Labels',{'Kalman AR(1)','Kalman AR(5)','Kalman AR(20)'});%,'Whisker',10)
title(['MSE between the Versions of' char(10) 'Kalman Filtering Based Channel Estimation'])
grid on;
set(gca,'XTickLabelRotation',45,'FontSize',12)
ylabel('MSE')

figure
boxplot([(MSE_Last2.'),(MSE_Last.'),(MSE_AR20_taps.'),(MSE_ML.'),(MSE_Preamble_Kalman_AR20.'),(MSE_Preamble_ML.'),(MSE_Preamble_Genie.')],'Notch',notch,'Labels',{'500ms Previous','100ms Previous','Kalman AR(20)','VVD-Current','Preamble-Kalman Combined','Preamble-VVD Combined','Preamble Based-Genie'});%,'Whisker',10)
title(['MSE between Different Estimation' char(10) 'Techniques and the Ground Truth'])
grid on;
set(gca,'XTickLabelRotation',45,'yscale','log','FontSize',12)
% set(gca,'yscale','log')
ylim([2e-7 6e-6])
ylabel('MSE')
yticks([2e-7,1e-6])
% yticklabels({'2x10\^{-7}','x = 5','x = 10'})
% set(gca,'xticklabel',{'1e-5','2e-5','3e-5','4e-5','5e-5'}
% notch = 'on';
% notch = 'off';

% l = legend('Set2 - Frame 497','Set5 - Frame 4266','Location','northwest');
% l.FontSize = 12;

%% PER Results Visualization
figure
boxplot([(PER_ML3_mean.'),(PER_ML1_mean.'),(PER_ML_mean.')],'Notch',notch,'Labels',{'VVD-100ms Future','VVD-33.3ms Future','VVD-Current'});%,'Whisker',5)
title(['Packet Error Rate of Versions' char(10) 'of Image Based Channel Estimation'])
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([1e-3 1])
ylabel('PER')

figure
boxplot([(PER_Kalman_AR1_mean.'),(PER_Kalman_AR5_mean.'),(PER_Kalman_AR20_mean.')],'Notch',notch,'Labels',{'Kalman AR(1)','Kalman AR(5)','Kalman AR(20)'});%,'Whisker',5)
title(['Packet Error Rate of Versions' char(10) 'of Kalman Filtering Based Channel Estimation'])
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([1e-3 1])
ylabel('PER')

figure
boxplot([(PER_NoEq_mean.'),(PER_Preamble_mean.'),(PER_Last2_mean.'),(PER_Last_mean.'),(PER_Kalman_AR20_mean.'),(PER_ML_mean.'),(PER_Preamble_mean_Kalman_AR20.'),(PER_Preamble_mean_ML.'),(PER_Preamble_Genie_mean.'),(PER_LS_mean.')],'Notch',notch,'Labels',{'Standard Decoding','Preamble Based','500ms Previous','100ms Previous','Kalman AR(20)','VVD-Current','Preamble-Kalman Combined','Preamble-VVD Combined','Preamble Based-Genie','Ground Truth'});%,'Whisker',5)
title('Packet Error Rate of All Techniques')
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
ylim([2e-3 5e-1])
ylabel('PER')

%% CER Results Visualization

figure
boxplot([(CER_ML3_mean.'),(CER_ML1_mean.'),(CER_ML_mean.')],'Notch',notch,'Labels',{'VVD-100ms Future','VVD-33.3ms Future','VVD-Current'});%,'Whisker',5)
title(['Chip Error Rate of Versions' char(10) 'of Image Based Channel Estimation'])
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([1e-3 1])
ylabel('CER')

figure
boxplot([(CER_Kalman_AR1_mean.'),(CER_Kalman_AR5_mean.'),(CER_Kalman_AR20_mean.')],'Notch',notch,'Labels',{'Kalman AR(1)','Kalman AR(5)','Kalman AR(20)'});%,'Whisker',5)
title(['Chip Error Rate of Versions' char(10) 'of Kalman Filtering Based Channel Estimation'])
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([1e-3 1])
ylabel('CER')

figure
boxplot([(CER_NoEq_mean.'),(CER_Preamble_mean.'),(CER_Last2_mean.'),(CER_Last_mean.'),(CER_Kalman_AR20_mean.'),(CER_ML_mean.'),(CER_Preamble_Kalman_AR20_mean.'),(CER_Preamble_ML_mean.'),(CER_Preamble_Genie_mean.'),(CER_LS_mean.')],'Notch',notch,'Labels',{'Standard Decoding','Preamble Based','500ms Previous','100ms Previous','Kalman AR(20)','VVD-Current','Preamble-Kalman Combined','Preamble-VVD Combined','Preamble Based-Genie','Ground Truth'});%,'Whisker',5)
title('Chip Error Rate of All Techniques')
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
ylim([9e-3 4e-1])
ylabel('CER')
