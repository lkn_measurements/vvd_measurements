These files are to run the ML model in matlab to use CPU. Average computation time is calculated as around 9.8 ms with Matlab 2018a with Intel Core i7-4700HQ CPU-2.40 GHz.
Matlab computation time can be calculated with the given code named as 'Average_Computation_Calculation.m'.


The same provided model and weights files can be used in jupyter notebook to observe the prediction time as well. The code segment for this is included in the provided jupyter notebook code.