addpath('./Chan_Est_Datasets/VVD_Ests/');
addpath('./Codes/Keras_Avg_Computation/');

%% Load image
depth=(imread('depth000958.png'));
depth1=(imread('depth002906.png'));
% figure;imshow(depth);
%% Downsampling & Cropping
subsampling_factor = 10;
depth_sub = depth(1:subsampling_factor:end,1:subsampling_factor:end,1);
depth_sub_crop = depth_sub(13:62,25:114);

depth_sub = depth1(1:subsampling_factor:end,1:subsampling_factor:end,1);
depth_sub_crop1 = depth_sub(13:62,25:114);
% figure;imshow(depth_sub_crop);
%% Load the predictions from Keras

% k = 8;
% load(strcat('set',int2str(k),'_11Tap_ML_predict_current.mat')) % Loaded as 'prediction_LS_11Tap'
% prediction_LS_11Tap_current = prediction_LS_11Tap;
% 
% only_prediction_file = strcat('Dataset_set',int2str(k),'_only_est_eq_phase.mat');
% load(only_prediction_file); 
%% Load Keras model in MATLAB
% model = importKerasNetwork('Model_Serkut.h5','WeightFile','Weights_Serkut.hdf5','OutputLayerType','regression');
model = importKerasNetwork('Nadam_4Conv2_Kernel3_5_5_3_11Tap_valid_6_test_8_complete_50x90_Dense_NoBN_averagepool_Complete.h5',...
    'WeightFile','weights-102.hdf5','OutputLayerType','regression');
model.Layers % Displays the architecture of the network load
%% Average computation time calculation
d(:,:,1) = depth_sub_crop;
d(:,:,2) = depth_sub_crop1;
clear time_pre
for i=1:1000
    m = mod(i,2)+1;
    tic
    h_LS = predict(model,d(:,:,m));
    ser2 = toc;
    time_pre(i) = ser2;
end
Avg_comp_t = mean(time_pre(2:end));

% Normalization factor is important to obtain the real estimation. The
% stored estimations are stored after normalization step. If you are
% directly using this code to predict please check regarding normalization
% factor for each set combination! (The pre-obtained kalman estimation .mat 
% files includes these normalization values in their dataset with same naming!)

% h_ML_matlab = complex(h_LS(:,1:11),h_LS(:,12:22))*Normalization_factor; 


% figure;
% hold on;plot(1:length(h_ML_matlab),abs(h_ML_matlab),'*'); 
