'Main.m' provides the main results obtained with the stored estimations for each method. This code loads all estimations and tries all for each received packet for comparison.
The results in the paper can be obtained with this code. Slight changes might be observed if the 'Estimation2Equalizer.m' code in the ''Helpers' folder is changed.
Approximately 5 hours is required for this code to be finished (with Matlab 2018a with Intel Core i7-4700HQ CPU-2.40 GHz)!

'VVD_Script.ipynb' provides the Keras codes for testing VVD. The code is self explanatory and uses the files that are provided in this dataset. However, be careful to change absolute paths to your PC configuration! 

'Dataset_Raw_Data_2_Chan_Est.m' provides the ground truth estimation by LS estimation method from the stored raw IQ samples of the received packets for each measurement take.
This code is provided to check the correctness of the obtained estimations from raw measurements.

'Aging_Codes' folder includes the codes that produces the results in aging plots. After running the codes plotting code for aging results should also work.

'Helpers' folder includes all codes that are used inside the main codes.

'Keras_ML_models' folder includes all train results for VVD for 3 variants. Each variant has its own result for each set combination. Set combination info is also provided. Each set combination result includes the prediction results for the VVD algorithm, the loss graphs, the saved model and weight as well.

'Average_Computation_Calculation.m' can be used to obtain the runtime of the trained ML algorithm on CPU with the help of the provided files in 'Keras_Avg_Computation' folder.

'Plot' folder is to plot the results in the paper. The folder includes stored results as well for quick plotting. But the codes should also work after running 'Main.m' or the codes in 'Aging_Codes' folder.

'npy_image_extraction.py' is to provide flexibility to the user to observe image frames of the dataset outside of jupyter notebook.
 With this code for example the depth images in the hypothesis testing figure (Figure 5) in the paper can be reobtained.

