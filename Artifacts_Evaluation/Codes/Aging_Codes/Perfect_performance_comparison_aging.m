%%

% addpath('../../Obtained_Ground_Chan_Est_Datasets/');
% addpath('../../Raw_Datasets/');
addpath('../Helpers/');
%% Specifies which sets are to be used as train and set
% Below set should be exactly the same with the sets of Machine learning
% algorithm that is to be compared
sets(1).set_train = [1,2,3,4,5,7,9,10,11,12,13,14,15];
sets(1).set_validation = 6;
sets(1).set_test = 8;

sets(2).set_train = [1,2,3,4,5,6,7,8,9,10,12,13,14];
sets(2).set_validation = 11;
sets(2).set_test = 15;

sets(3).set_train = [1,2,3,4,5,6,7,8,10,11,12,13,15];
sets(3).set_validation = 14;
sets(3).set_test = 9;

sets(4).set_train = [1,3,4,6,7,8,9,10,11,12,13,14,15];
sets(4).set_validation = 5;
sets(4).set_test = 2;

sets(5).set_train = [1,2,3,5,6,7,8,9,10,11,13,14,15];
sets(5).set_validation = 12;
sets(5).set_test = 4;

sets(6).set_train = [2,3,4,5,6,7,8,9,11,12,13,14,15];
sets(6).set_validation = 10;
sets(6).set_test = 1;

sets(7).set_train = [1,2,3,4,5,7,8,10,11,12,13,14,15];
sets(7).set_validation = 9;
sets(7).set_test = 6;

sets(8).set_train = [1,2,4,5,6,7,8,9,10,11,12,14,15];
sets(8).set_validation = 13;
sets(8).set_test = 3;

sets(9).set_train = [1,2,3,4,6,7,9,10,11,12,13,14,15];
sets(9).set_validation = 8;
sets(9).set_test = 5;

sets(15).set_train = [1,2,3,5,6,8,9,10,11,12,13,14,15];
sets(15).set_validation = 4;
sets(15).set_test = 7;

sets(11).set_train = [1,2,4,5,6,7,8,9,11,12,13,14,15];
sets(11).set_validation = 3;
sets(11).set_test = 10;

sets(12).set_train = [1,2,3,4,5,6,8,9,10,12,13,14,15];
sets(12).set_validation = 7;
sets(12).set_test = 11;

sets(13).set_train = [1,2,3,4,5,6,7,8,9,10,11,14,15];
sets(13).set_validation = 13;
sets(13).set_test = 12;

sets(14).set_train = [1,3,4,5,6,7,8,9,10,11,12,14,15];
sets(14).set_validation = 2;
sets(14).set_test = 13;

sets(10).set_train = [2,3,4,5,6,7,8,9,10,11,12,13,15];
sets(10).set_validation = 1;
sets(10).set_test = 14;


% left_count_packet = 0;
PER_LS_org_mean2 = [];
PER_LS_0_1_mean2 = [];
PER_LS_0_5_mean2 = [];
PER_LS_1_0_mean2 = [];
PER_LS_2_0_mean2 = [];
PER_LS_5_0_mean2 = [];
PER_LS_10_0_mean2 = [];
PER_LS_20_0_mean2 = [];

CER_LS_org_mean2 = [];
CER_LS_0_1_mean2 = [];
CER_LS_0_5_mean2 = [];
CER_LS_1_0_mean2 = [];
CER_LS_2_0_mean2 = [];
CER_LS_5_0_mean2 = [];
CER_LS_10_0_mean2 = [];
CER_LS_20_0_mean2 = [];

version_len = 15;
for set_v=1:version_len
disp(['Set Version: ' num2str(set_v)]);
set_train = sets(set_v).set_train;
set_validation = sets(set_v).set_validation;
set_test = sets(set_v).set_test;

% Load predictions and raw signal
i = set_test; % Set number that is to be compared
only_prediction_file = strcat('../../Obtained_Ground_Chan_Est_Datasets/Dataset_set',int2str(i),'_only_est_eq_phase.mat');
load(only_prediction_file);     % Loaded as 'Dataset_set'

% Use below code for automized calculation for different sets O.w.Loaded as i.e. 'Dataset_set8'
raw_signal_file = strcat('../../Raw_Datasets/Dataset_set',int2str(i),'.mat');
raw_dataset = load(raw_signal_file);          
struct_str_dataset = fieldnames(raw_dataset);
struct_str_dataset=struct_str_dataset{1};
raw_dataset = raw_dataset.(struct_str_dataset);

% Parameters that are to be used
spc = 8;
decimationFactor = 1;
set_length = size(Dataset_set,2);

% Clear variables if nmber of observed packets is to be changed

clear BitErr_LS_org_sum
clear BitErr_LS_0_1_sum
clear BitErr_LS_0_5_sum
clear BitErr_LS_1_0_sum
clear BitErr_LS_2_0_sum
clear BitErr_LS_5_0_sum 
clear BitErr_LS_10_0_sum 
clear BitErr_LS_20_0_sum 

clear ChipErr_LS_org_sum
clear ChipErr_LS_0_1_sum
clear ChipErr_LS_0_5_sum
clear ChipErr_LS_1_0_sum
clear ChipErr_LS_2_0_sum
clear ChipErr_LS_5_0_sum
clear ChipErr_LS_10_0_sum
clear ChipErr_LS_20_0_sum

clear PER_Count_LS_org
clear PER_Count_LS_0_1
clear PER_Count_LS_0_5
clear PER_Count_LS_1_0
clear PER_Count_LS_2_0
clear PER_Count_LS_5_0
clear PER_Count_LS_10_0
clear PER_Count_LS_20_0


clear p_chanEst_4MSE

waveform_for_preamble = lrwpan.PHYGeneratorOQPSK(raw_dataset(1).MPDU, spc/decimationFactor, '2450 MHz'); % MPDU
preamble_waveform = waveform_for_preamble(1:1280,1);

disp('Start Comparison Calculations');

packet_num_range = 201:set_length; % 201:set_length
for packet_num = packet_num_range
    disp(['Packet Num: ' num2str(packet_num) ' / ' num2str(packet_num_range(end))]);

    filteredOQPSK = raw_dataset(packet_num).Raw_Packet_Signal;
    
    Or_MPDU = raw_dataset(packet_num).MPDU;
    Or_preamble_start = raw_dataset(packet_num).synch_preamble_start;
    Or_phase_amb = raw_dataset(packet_num).synch_phase_ambiguity;
    
    coarseFrequencyOffset = Dataset_set(packet_num).CFO;
    Or_Corr_angle = Dataset_set(packet_num).phase_correction;
    frame_num = Dataset_set(packet_num).Frame_Number;
    
    % Obtained estimations
    
    p_LS_org = Dataset_set(packet_num).LS_11Tap;
    p_LS_0_1 = Dataset_set(packet_num-1).LS_11Tap_Corr;
    p_LS_0_5 = Dataset_set(packet_num-5).LS_11Tap_Corr;
    p_LS_1_0 = Dataset_set(packet_num-10).LS_11Tap_Corr;
    p_LS_2_0 = Dataset_set(packet_num-20).LS_11Tap_Corr;
    p_LS_5_0 = Dataset_set(packet_num-50).LS_11Tap_Corr;
    p_LS_10_0 = Dataset_set(packet_num-100).LS_11Tap_Corr;
    p_LS_20_0 = Dataset_set(packet_num-200).LS_11Tap_Corr;


    
    p_chanEst_4MSE(:,packet_num) = Dataset_set(packet_num).LS_11Tap_Corr;

    %% Below is for the Cancellation and Chip Error Rate Comparison part
    
    % Frequency correction to the signal
    
    freqoffset = comm.PhaseFrequencyOffset('SampleRate',spc*1e6/decimationFactor,'FrequencyOffsetSource','Input port');
    coarseCompensatedOQPSK = freqoffset(filteredOQPSK,-coarseFrequencyOffset);

    %% Estimation Phase offsets are corrected here!

    p_LS_0_1 = Estimation_Phase_Corrector2(p_LS_0_1,preamble_waveform,coarseCompensatedOQPSK);
    p_LS_0_5 = Estimation_Phase_Corrector2(p_LS_0_5,preamble_waveform,coarseCompensatedOQPSK);
    p_LS_1_0 = Estimation_Phase_Corrector2(p_LS_1_0,preamble_waveform,coarseCompensatedOQPSK);
    p_LS_2_0 = Estimation_Phase_Corrector2(p_LS_2_0,preamble_waveform,coarseCompensatedOQPSK);
    p_LS_5_0 = Estimation_Phase_Corrector2(p_LS_5_0,preamble_waveform,coarseCompensatedOQPSK);
    p_LS_10_0 = Estimation_Phase_Corrector2(p_LS_10_0,preamble_waveform,coarseCompensatedOQPSK);
    p_LS_20_0 = Estimation_Phase_Corrector2(p_LS_20_0,preamble_waveform,coarseCompensatedOQPSK);

    %% Equalizers from corrected estimations are obtained

    c_LS_org = Estimation2Equalizer(p_LS_org);
    c_LS_0_1 = Estimation2Equalizer(p_LS_0_1);
    c_LS_0_5 = Estimation2Equalizer(p_LS_0_5);
    c_LS_1_0 = Estimation2Equalizer(p_LS_1_0);
    c_LS_2_0 = Estimation2Equalizer(p_LS_2_0);
    c_LS_5_0 = Estimation2Equalizer(p_LS_5_0);
    c_LS_10_0 = Estimation2Equalizer(p_LS_10_0);
    c_LS_20_0 = Estimation2Equalizer(p_LS_20_0);
    
    %% Equalized Data acquisition

    LS_org_Eq_data = filter(c_LS_org,1,coarseCompensatedOQPSK);
    LS_0_1_Eq_data = filter(c_LS_0_1,1,coarseCompensatedOQPSK);
    LS_0_5_Eq_data = filter(c_LS_0_5,1,coarseCompensatedOQPSK);
    LS_1_0_Eq_data = filter(c_LS_1_0,1,coarseCompensatedOQPSK);
    LS_2_0_Eq_data = filter(c_LS_2_0,1,coarseCompensatedOQPSK);
    LS_5_0_Eq_data = filter(c_LS_5_0,1,coarseCompensatedOQPSK);
    LS_10_0_Eq_data = filter(c_LS_10_0,1,coarseCompensatedOQPSK);
    LS_20_0_Eq_data = filter(c_LS_20_0,1,coarseCompensatedOQPSK);

    %% Decode signals obtained from different methods, Chip error rate is obtained here!!

    [MPDU_LS_org,phase_amb_LS_org,preamble_start_idx_LS_org, BitErr_LS_org, ChipErr_LS_org, PacketErr_LS_org] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(LS_org_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    [MPDU_LS_0_1,phase_amb_LS_0_1,preamble_start_idx_LS_0_1, BitErr_LS_0_1, ChipErr_LS_0_1, PacketErr_LS_0_1] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(LS_0_1_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    [MPDU_LS_0_5,phase_amb_LS_0_5,preamble_start_idx_LS_0_5, BitErr_LS_0_5, ChipErr_LS_0_5, PacketErr_LS_0_5] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(LS_0_5_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    [MPDU_LS_1_0,phase_amb_LS_1_0,preamble_start_idx_LS_1_0, BitErr_LS_1_0, ChipErr_LS_1_0, PacketErr_LS_1_0] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(LS_1_0_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    [MPDU_LS_2_0,phase_amb_LS_2_0,preamble_start_idx_LS_2_0, BitErr_LS_2_0, ChipErr_LS_2_0, PacketErr_LS_2_0] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(LS_2_0_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    [MPDU_LS_5_0,phase_amb_LS_5_0,preamble_start_idx_LS_5_0, BitErr_LS_5_0, ChipErr_LS_5_0, PacketErr_LS_5_0] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(LS_5_0_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    [MPDU_LS_10_0,phase_amb_LS_10_0,preamble_start_idx_LS_10_0, BitErr_LS_10_0, ChipErr_LS_10_0, PacketErr_LS_10_0] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(LS_10_0_Eq_data,spc/decimationFactor,...
        Or_MPDU);
    [MPDU_LS_20_0,phase_amb_LS_20_0,preamble_start_idx_LS_20_0, BitErr_LS_20_0, ChipErr_LS_20_0, PacketErr_LS_20_0] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp(LS_20_0_Eq_data,spc/decimationFactor,...
        Or_MPDU);

    %% Observed Bit Error Rates and Chip Error Rates are summed here to be averaged at the end!
    
    
    BitErr_LS_org_sum(packet_num-packet_num_range(1)+1) = BitErr_LS_org;
    ChipErr_LS_org_sum(packet_num-packet_num_range(1)+1) = ChipErr_LS_org;
    
    BitErr_LS_0_1_sum(packet_num-packet_num_range(1)+1) = BitErr_LS_0_1;
    ChipErr_LS_0_1_sum(packet_num-packet_num_range(1)+1) = ChipErr_LS_0_1;
    
    BitErr_LS_0_5_sum(packet_num-packet_num_range(1)+1) = BitErr_LS_0_5;
    ChipErr_LS_0_5_sum(packet_num-packet_num_range(1)+1) = ChipErr_LS_0_5;
    
    BitErr_LS_1_0_sum(packet_num-packet_num_range(1)+1) = BitErr_LS_1_0;
    ChipErr_LS_1_0_sum(packet_num-packet_num_range(1)+1) = ChipErr_LS_1_0;
    
    BitErr_LS_2_0_sum(packet_num-packet_num_range(1)+1) = BitErr_LS_2_0;
    ChipErr_LS_2_0_sum(packet_num-packet_num_range(1)+1) = ChipErr_LS_2_0;
    
    BitErr_LS_5_0_sum(packet_num-packet_num_range(1)+1) = BitErr_LS_5_0;
    ChipErr_LS_5_0_sum(packet_num-packet_num_range(1)+1) = ChipErr_LS_5_0;
    
    BitErr_LS_10_0_sum(packet_num-packet_num_range(1)+1) = BitErr_LS_10_0;
    ChipErr_LS_10_0_sum(packet_num-packet_num_range(1)+1) = ChipErr_LS_10_0;
    
    BitErr_LS_20_0_sum(packet_num-packet_num_range(1)+1) = BitErr_LS_20_0;
    ChipErr_LS_20_0_sum(packet_num-packet_num_range(1)+1) = ChipErr_LS_20_0;

    %% Packet Error Rate

    PER_Count_LS_org(packet_num-packet_num_range(1)+1) = PacketErr_LS_org;
    PER_Count_LS_0_1(packet_num-packet_num_range(1)+1) = PacketErr_LS_0_1;
    PER_Count_LS_0_5(packet_num-packet_num_range(1)+1) = PacketErr_LS_0_5;
    PER_Count_LS_1_0(packet_num-packet_num_range(1)+1) = PacketErr_LS_1_0;
    PER_Count_LS_2_0(packet_num-packet_num_range(1)+1) = PacketErr_LS_2_0;
    PER_Count_LS_5_0(packet_num-packet_num_range(1)+1) = PacketErr_LS_5_0;
    PER_Count_LS_10_0(packet_num-packet_num_range(1)+1) = PacketErr_LS_10_0;
    PER_Count_LS_20_0(packet_num-packet_num_range(1)+1) = PacketErr_LS_20_0;
    
end
%% Getting an average over the obtained values part 1

PER_LS_org_mean(set_v) = mean(PER_Count_LS_org);
PER_LS_0_1_mean(set_v) = mean(PER_Count_LS_0_1);
PER_LS_0_5_mean(set_v) = mean(PER_Count_LS_0_5);
PER_LS_1_0_mean(set_v) = mean(PER_Count_LS_1_0);
PER_LS_2_0_mean(set_v) = mean(PER_Count_LS_2_0);
PER_LS_5_0_mean(set_v) = mean(PER_Count_LS_5_0);
PER_LS_10_0_mean(set_v) = mean(PER_Count_LS_10_0);
PER_LS_20_0_mean(set_v) = mean(PER_Count_LS_20_0);

%% Mean with grouping by 10 every Packet
Group_size = 10;
for ser=1:Group_size:size(PER_Count_LS_org,2)-Group_size
    PER_LS_org_mean2 = [PER_LS_org_mean2,mean(PER_Count_LS_org(ser:ser+Group_size-1))];
    PER_LS_0_1_mean2 = [PER_LS_0_1_mean2,mean(PER_Count_LS_0_1(ser:ser+Group_size-1))];
    PER_LS_0_5_mean2 = [PER_LS_0_5_mean2,mean(PER_Count_LS_0_5(ser:ser+Group_size-1))];
    PER_LS_1_0_mean2 = [PER_LS_1_0_mean2,mean(PER_Count_LS_1_0(ser:ser+Group_size-1))];
    PER_LS_2_0_mean2 = [PER_LS_2_0_mean2,mean(PER_Count_LS_2_0(ser:ser+Group_size-1))];
    PER_LS_5_0_mean2 = [PER_LS_5_0_mean2,mean(PER_Count_LS_5_0(ser:ser+Group_size-1))];
    PER_LS_10_0_mean2 = [PER_LS_10_0_mean2,mean(PER_Count_LS_10_0(ser:ser+Group_size-1))];
    PER_LS_20_0_mean2 = [PER_LS_20_0_mean2,mean(PER_Count_LS_20_0(ser:ser+Group_size-1))];
       
    CER_LS_org_mean2 = [CER_LS_org_mean2,mean(ChipErr_LS_org_sum(ser:ser+Group_size-1))];
    CER_LS_0_1_mean2 = [CER_LS_0_1_mean2,mean(ChipErr_LS_0_1_sum(ser:ser+Group_size-1))];
    CER_LS_0_5_mean2 = [CER_LS_0_5_mean2,mean(ChipErr_LS_0_5_sum(ser:ser+Group_size-1))];
    CER_LS_1_0_mean2 = [CER_LS_1_0_mean2,mean(ChipErr_LS_1_0_sum(ser:ser+Group_size-1))];
    CER_LS_2_0_mean2 = [CER_LS_2_0_mean2,mean(ChipErr_LS_2_0_sum(ser:ser+Group_size-1))];
    CER_LS_5_0_mean2 = [CER_LS_5_0_mean2,mean(ChipErr_LS_5_0_sum(ser:ser+Group_size-1))];
    CER_LS_10_0_mean2 = [CER_LS_10_0_mean2,mean(ChipErr_LS_10_0_sum(ser:ser+Group_size-1))];
    CER_LS_20_0_mean2 = [CER_LS_20_0_mean2,mean(ChipErr_LS_20_0_sum(ser:ser+Group_size-1))];
end

%%

BER_LS_org_mean(set_v) = mean(BitErr_LS_org_sum);
BER_LS_0_1_mean(set_v) = mean(BitErr_LS_0_1_sum);
BER_LS_0_5_mean(set_v) = mean(BitErr_LS_0_5_sum);
BER_LS_1_0_mean(set_v) = mean(BitErr_LS_1_0_sum);
BER_LS_2_0_mean(set_v) = mean(BitErr_LS_2_0_sum);
BER_LS_5_0_mean(set_v) = mean(BitErr_LS_5_0_sum);
BER_LS_10_0_mean(set_v) = mean(BitErr_LS_10_0_sum);
BER_LS_20_0_mean(set_v) = mean(BitErr_LS_20_0_sum);

CER_LS_org_mean(set_v) = mean(ChipErr_LS_org_sum);
CER_LS_0_1_mean(set_v) = mean(ChipErr_LS_0_1_sum);
CER_LS_0_5_mean(set_v) = mean(ChipErr_LS_0_5_sum);
CER_LS_1_0_mean(set_v) = mean(ChipErr_LS_1_0_sum);
CER_LS_2_0_mean(set_v) = mean(ChipErr_LS_2_0_sum);
CER_LS_5_0_mean(set_v) = mean(ChipErr_LS_5_0_sum);
CER_LS_10_0_mean(set_v) = mean(ChipErr_LS_10_0_sum);
CER_LS_20_0_mean(set_v) = mean(ChipErr_LS_20_0_sum);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% MSE comparison of the estimations
start = 201; %AR_len+2;
stop = size(p_chanEst_4MSE,2);
tap_s = size(p_chanEst_4MSE,1)*2; % *2 for real imag separation


Chan_est2_mse = [real(p_chanEst_4MSE.'),imag(p_chanEst_4MSE.')];

MSE_LS_0_1(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Chan_est2_mse(start-1:stop-1,:)).^2)/((stop-start+1)*tap_s);
MSE_LS_0_5(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Chan_est2_mse(start-5:stop-5,:)).^2)/((stop-start+1)*tap_s);
MSE_LS_1_0(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Chan_est2_mse(start-10:stop-10,:)).^2)/((stop-start+1)*tap_s);
MSE_LS_2_0(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Chan_est2_mse(start-20:stop-20,:)).^2)/((stop-start+1)*tap_s);
MSE_LS_5_0(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Chan_est2_mse(start-50:stop-50,:)).^2)/((stop-start+1)*tap_s);
MSE_LS_10_0(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Chan_est2_mse(start-100:stop-100,:)).^2)/((stop-start+1)*tap_s);
MSE_LS_20_0(set_v) = sumabs((Chan_est2_mse(start:stop,:)-Chan_est2_mse(start-200:stop-200,:)).^2)/((stop-start+1)*tap_s);

set_data_count(set_v) = size(packet_num_range,2);
end


%% Found Results to Visualize

%% MSE RESULTS (dB)
notch = 'off';

figure
boxplot([(MSE_LS_0_1.'),(MSE_LS_0_5.'),(MSE_LS_1_0.'),(MSE_LS_2_0.'),(MSE_LS_5_0.'),(MSE_LS_10_0.'),(MSE_LS_20_0.')],'Notch',notch,'Labels',{'0.1s Previous','0.5s Previous','1s Previous','2s Previous','5s Previous','10s Previous','20s Previous'});%,'Whisker',10)
title(['MSE between Ground Truth' char(10) 'and the Ground Truth with Age'])
grid on;
set(gca,'XTickLabelRotation',45,'FontSize',12)
ylabel('MSE')


%% PER RESULTS

figure
boxplot([(PER_LS_org_mean.'),(PER_LS_0_1_mean.'),(PER_LS_0_5_mean.'),(PER_LS_1_0_mean.'),(PER_LS_2_0_mean.'),(PER_LS_5_0_mean.'),(PER_LS_10_0_mean.'),(PER_LS_20_0_mean.')],'Notch',notch,'Labels',{'Original','0.1s Previous','0.5s Previous','1s Previous','2s Previous','5s Previous','10s Previous','20s Previous'});%,'Whisker',5)
title('Packet Error Rate of Ground Truth with Age')
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([2e-3 5e-1])
ylabel('PER')
%% CER RESULTS

figure
boxplot([(CER_LS_org_mean.'),(CER_LS_0_1_mean.'),(CER_LS_0_5_mean.'),(CER_LS_1_0_mean.'),(CER_LS_2_0_mean.'),(CER_LS_5_0_mean.'),(CER_LS_10_0_mean.'),(CER_LS_20_0_mean.')],'Notch',notch,'Labels',{'Original','0.1s Previous','0.5s Previous','1s Previous','2s Previous','5s Previous','10s Previous','20s Previous'});%,'Whisker',5)
title('Chip Error Rate of Ground Truth with Age')
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([9e-3 4e-1])
ylabel('CER')
