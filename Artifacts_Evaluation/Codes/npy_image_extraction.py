# -*- coding: utf-8 -*-
"""
Created on Sat Dec  1 18:20:16 2018

@author: Serko
"""

import numpy as np
import matplotlib.pyplot as plt
import math
import scipy.io as spio 

def image_extract(Img_format,subsampling_factor_str,set_name,Frame_Num):
    dataset_path_save = 'F:\\Git\\Codes\\Artifacts_Evaluation\\Images\\Set_by_set\\' + Img_format +'_Sub'+subsampling_factor_str+'_Sets_'+set_name+'.npy'
    Frames_set = np.load(dataset_path_save)
    print(Frames_set.shape)
    
    set_mat = spio.loadmat('F:\\Git\\Codes\\Artifacts_Evaluation\\Obtained_Ground_Chan_Est_Datasets\\Dataset_set'+set_name+'_only_est_eq_phase.mat', struct_as_record=False,squeeze_me=True)
    mat_dataset = np.copy(set_mat['Dataset_set'])
    print(mat_dataset.shape)
    
    first_frame = mat_dataset[0].Frame_Number 
    print(first_frame)
    
    diff_frame = (Frame_Num-(first_frame))
    Packet_num = math.ceil((diff_frame)/3)
    if Packet_num<0:
        Packet_num=0
      
    if Frame_Num<first_frame-30:
        raise Exception('Frame_Num must be greater than or equal to: {}'.format(first_frame-30))
        
    image = Frames_set[Packet_num,0,:,:,:]
    
    if image.shape[2]==1:
        image1 = np.copy(image[:,:,0])
    return image1

Img_format = 'Depth'                #  'Depth'
subsampling_factor_str = '10'       # '4' and '10' is provided only
set_name = '5'                      # from '1' up to '15'
Frame_Num = 4266                     # Starts arbitrary as stored from first packet's frame-30. Code throws error if non-existing frame number is given

'Not every frame are obtained with this method as 3 frames exists per packet! All the frames are in the dataset in Images\\Set_[setnum] folders but not in the versions that are inside Images\\Set_by_set  folder'
image = image_extract(Img_format,subsampling_factor_str,set_name,Frame_Num)

plt.imshow(image,cmap="gray")
plt.show()