function p_LS = Estimation_Phase_Corrector2(p_LS,Preamble_waveform,coarseCompensatedOQPSK)

spc = 8;
decimationFactor = 1;

Received_imitated = filter(p_LS(:,1),1,Preamble_waveform);

[acor,lag] = xcorr(coarseCompensatedOQPSK,Preamble_waveform);
[~,I]=max(abs(acor));
Start_ind=I+lag(1);

% figure;
% plot(lag,abs(acor));
% title('Cross Correlation of SHR and Synchronized Signal');

[acor,lag] = xcorr(coarseCompensatedOQPSK(Start_ind:Start_ind+1279),Received_imitated);
[~,I]=max(abs(acor));
Start_imitation=I+lag(1);

% serk = acor(I);
% ang_serk_sig = angle(serk);
% corr_phase_ang = (ang_serk_sig/pi)*180;

ind = 1-Start_imitation;
phase_diff = angle(coarseCompensatedOQPSK(Start_ind:Start_ind+1279-ind).*conj(Received_imitated(1+ind:end))); 

% figure;
% plot(1:size(phase_diff),phase_diff);

% mean_phase = pi/4-ang_ref;        % All estimation phases are referenced to pi/4
mean_phase = mean(phase_diff(1000:end));
corr_phase = (mean_phase/pi)*180;

phaseoffset = comm.PhaseFrequencyOffset('SampleRate',spc*1e6/decimationFactor,'PhaseOffset',corr_phase);

p_LS(:,1) = phaseoffset(p_LS(:,1));

% Below does it twice as it is observed that when the phase is around -pi:pi
% It cannot correct it with one perform. This could (should!) be optimized
% instead of performing twice but left for future work as this is working
% currently!
phaseoffset = comm.PhaseFrequencyOffset('SampleRate',spc*1e6/decimationFactor,'PhaseOffset',corr_phase);
Received_imitated = phaseoffset(Received_imitated(1:end));

phase_diff = angle(coarseCompensatedOQPSK(Start_ind:Start_ind+1279-ind).*conj(Received_imitated(1+ind:end))); 

% figure;
% plot(1:size(phase_diff),phase_diff);
mean_phase2 = mean(phase_diff(1000:end));
corr_phase2 = (mean_phase2/pi)*180;

phaseoffset = comm.PhaseFrequencyOffset('SampleRate',spc*1e6/decimationFactor,'PhaseOffset',corr_phase2);

p_LS(:,1) = phaseoffset(p_LS(:,1));
end