function c_LS = Estimation2Equalizer(p_LS)
L1 = 1; % 1 Org
% Changing below numbers could change the obtained results slightly!
N1 = 50; N2 = 50; % Equalizer length is N1+N2+1   
p_LS2 = p_LS(:,1);
P = convmtx(p_LS2,N1+N2+1); % Convolution matrix 
u_ZF = zeros(size(P,1),1); % u vector (zero forcing)
u_ZF(L1+N1+1) = 1; % Puts the one in the place corresponding to channel delay
c_LS = ((P'*P)\(P'))*u_ZF;
end