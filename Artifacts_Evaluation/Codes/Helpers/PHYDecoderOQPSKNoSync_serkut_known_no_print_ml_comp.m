function [MPDU,phase,preambleStart,BitErrorRate,ChipErrorRate,PacketErrors] = PHYDecoderOQPSKNoSync_serkut_known_no_print_ml_comp( waveform, OSR, Or_MPDU, varargin )
%PHYDECODERSYNCEDOQPSK Receive-side physical layer for synchronized OQPSK signals
%   MPDU = PHYDECODERSYNCEDOQPSK( SYNCHRONIZED ) accepts the QPSK
%   symbols SYNCHRONIZED that have been passed to a matched filter, a coarse
%   frequency compensator, a fine frequency compensator and a symbol
%   synchronizer. Then it performs preamble detection, despreading and
%   symbol to bit mapping.

%   [MPDU,phase] ...
%   returns also found phase ambiguity (in pi (i.e. phase=4.7124 ==> 3*pi/2)) that is caused by fine frequency
%   compensation
%   Copyright 2017 The MathWorks, Inc. 

%% Validation

% % OSR specification
% OSR = lrwpan.internal.osrValidation(nargin, varargin{1});

% frequency band specification
if nargin >= 6
  band = validatestring(varargin{2},{'780MHz', '868MHz','915MHz', '2450MHz'},'','frequency band');
else
  band = '2450MHz';
end
chipLen = 16;
PHYHeaderLen = 6; % 6 octets: 4 octets for Preamble, 1 octet for SFD, 1 octet for PHR (Frame Length Indicator)
if strcmp(band, '2450MHz')
  chipLen = 32;
	chipMap = flipud(...
      [1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0;
       1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0;
       0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0;
       0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1;
       0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1;
       0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0;
       1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1;
       1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1;
       1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1;
       1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1;
       0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1;
       0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0;
       0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0;
       0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1;
       1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0;
       1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0]);
     % flipud as min returns first found element, so avoid false preamble
     % detection for purely random data
else
  chipMap   = flipud([0 0 1 1 1 1 1 0 0 0 1 0 0 1 0 1;
                      0 1 0 0 1 1 1 1 1 0 0 0 1 0 0 1;
                      0 1 0 1 0 0 1 1 1 1 1 0 0 0 1 0;
                      1 0 0 1 0 1 0 0 1 1 1 1 1 0 0 0;
                      0 0 1 0 0 1 0 1 0 0 1 1 1 1 1 0;
                      1 0 0 0 1 0 0 1 0 1 0 0 1 1 1 1;
                      1 1 1 0 0 0 1 0 0 1 0 1 0 0 1 1;
                      1 1 1 1 1 0 0 0 1 0 0 1 0 1 0 0;
                      0 1 1 0 1 0 1 1 0 1 1 1 0 0 0 0;
                      0 0 0 1 1 0 1 0 1 1 0 1 1 1 0 0;
                      0 0 0 0 0 1 1 0 1 0 1 1 0 1 1 1;
                      1 1 0 0 0 0 0 1 1 0 1 0 1 1 0 1;
                      0 1 1 1 0 0 0 0 0 1 1 0 1 0 1 1;
                      1 1 0 1 1 1 0 0 0 0 0 1 1 0 1 0;
                      1 0 1 1 0 1 1 1 0 0 0 0 0 1 1 0;
                      1 0 1 0 1 1 0 1 1 1 0 0 0 0 0 1]);
end
%% O-QPSK demodululation (part 1)
re = real(waveform( 1      :end-round(OSR/2))); % remove "dummy" real part at end
im = imag(waveform( 1+round(OSR/2):end));       % remove "dummy" imag part at start

addition = repmat(re(1),mod(OSR-mod(size(re,1),OSR),OSR),1);

filteredReal = intdump([addition;re], OSR);
filteredImag = intdump([addition;im], OSR);
b4_rotate = complex(filteredReal,filteredImag);
original_chips = PHYGeneratorOQPSK_for_chip_error_rate(Or_MPDU);
%% Resolve phase ambiguity caused by fine frequency compensation
for phase = 0:pi/2:3*pi/2
    MPDU=1;
    rotated = b4_rotate*exp(1i*phase);
    
    %% O-QPSK demodululation
    temp = [transpose(real(rotated (1:end))); transpose(imag(rotated (1:end)))];
    demodulated = temp(:) > 0; % Slicing, convert [-1 1] to [0 1]
    %% Preamble detection
    % Exhaustive sliding window for detection of first preamble
    for preambleStart = 1:(length(demodulated)-8*chipLen+1)
        % The preamble is 32 (despreaded) zeros. This is 8 symbols (4 octets),
        % which corresponds to 8*chipLen spreaded bits.
        preambleFound = true;
        MPDU=1;
        
        for chipNo = 1:8                % each chip should give symbol 0, which is 4 zero bits
            
            thisChip = demodulated(preambleStart + (chipNo-1)*chipLen : preambleStart-1+chipNo*chipLen);
            symbol = despread(thisChip, chipMap);
            if symbol ~= 0
                % preamble detection failed
                preambleFound = false;
                break;                  % break from detecting pramble at this start index
            end
        end
        
        % Preamble detection results:
        if ~preambleFound               % preamble not found at this index
            MPDU = [];
            continue;                   % continue with the next one to avoid unnecessary operations below
        end
%         fprintf('Found preamble of OQPSK PHY.\n');
        
        %% Start-of-frame delimiter (SFD) detection
        SFD = [1 1 1 0 0 1 0 1];
        % SFD is 2 symbols, i.e., 2 chip sequences
        sfdStart = preambleStart + 4*8*8;
        % 1st chip sequence
        thisChip1 = demodulated(sfdStart : sfdStart -1+chipLen);
        symbol1 = despread(thisChip1, chipMap);
        % 2nd chip sequence
        thisChip2 = demodulated(sfdStart  + chipLen : sfdStart-1 + 2*chipLen);
        symbol2 = despread(thisChip2, chipMap);
        if ~isequal(SFD, [de2bi(symbol1, 4) de2bi(symbol2, 4)])
            MPDU = [];
            continue;
        end
%         fprintf('Found start-of-frame delimiter (SFD) of OQPSK PHY.\n');
        
        %% PHY Header (PHR)
        preambleLen = 4*8;  % 4 octets
        SFDLen = 8;         % 1 octet
        PHRLen = 8;         % 1 octet
        offset = preambleLen + SFDLen + PHRLen;
        
        phrStart = preambleStart + 2*chipLen*(preambleLen+SFDLen)/8;
        phrChips = demodulated(phrStart : phrStart+2*chipLen-1);
        symbolA = despread(phrChips(1:chipLen),     chipMap);
        symbolB = despread(phrChips(chipLen+1:end), chipMap);
        
        % PHR contains the MPDU length
        PHR = [de2bi(symbolA, 4) de2bi(symbolB, 4)];
        frameLen = bi2de(PHR(1:7)); % number of octets
        
        if ((frameLen+PHYHeaderLen)*8)*chipLen/4 > length(demodulated) - preambleStart
            MPDU = [];
            BitErrorRate = 1;
            PacketErrors = 1;
            ChipErrorRate = 1;
            return;
        end
        
        % if preamble was found do not try remaining rotations
        % and MPDU could be obtained  (Serkut)
        if preambleFound && ~isequal(MPDU,[])
            break;  % break from resolution of phase ambiguity
        end
    end
    if preambleFound && ~isequal(MPDU,[])
        break;      % break from resolution of phase ambiguity
    end
end
if isequal(MPDU,[]) || frameLen~=size(Or_MPDU,1)/8
    BitErrorRate = 1;
    PacketErrors = 1;
    ChipErrorRate = 1;
    return;
end

%% Despreading
bits = zeros(4, frameLen*8/4);
for chipNo = 1:frameLen*8/4
  
  %% Chip to symbol mapping
  thisChip = demodulated(preambleStart+offset*chipLen/4+(chipNo-1)*chipLen:preambleStart-1+offset*chipLen/4+chipNo * chipLen);
  % find the chip sequence that looks the most like the received (minimum number of bit errors)
  symbol = despread(thisChip, chipMap);
  %% Symbol to bit mapping
  bits(:, chipNo) = de2bi(symbol, 4);
end
  
% Output despreaded bits
MPDU = bits(:);

% Below shows the % version of it to later use as Bit Error Probability
BitErrorRate = biterr(Or_MPDU,MPDU)/size(Or_MPDU,1); 
if BitErrorRate == 0
    PacketErrors = 0;
else
    PacketErrors = 1;
end
signal_chip_part = demodulated((preambleStart):(preambleStart-1+offset*chipLen/4+(frameLen*8/4)*chipLen));

% Below shows the % version of it to later use as Chip Error Probability
ChipErrorRate = biterr(original_chips,signal_chip_part)/size(original_chips,1); 
end 

function symbol = despread(chip, chipMap)
  % Find the closest chip sequence
  [~, symbol] = min(sum(xor(chip', chipMap), 2));
  % substract from size to cancel flupud:
  symbol = size(chipMap, 1) - symbol; % result follows 0-based indexing
end