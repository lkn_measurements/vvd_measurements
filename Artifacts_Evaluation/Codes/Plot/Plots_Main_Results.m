%% Load Required data
% save('Plot_Original_Data_Save.mat','MSE_ML3','MSE_ML1','MSE_AR1_taps','MSE_AR5_taps','MSE_Last2',...
%     'MSE_Last','MSE_AR20_taps','MSE_ML','MSE_Preamble','MSE_Preamble_Kalman_AR20',...
%     'MSE_Preamble_ML','MSE_Preamble_Genie','PER_ML3_mean','PER_ML1_mean','PER_Kalman_AR1_mean',...
%     'PER_Kalman_AR5_mean','PER_NoEq_mean','PER_Preamble_mean','PER_Last2_mean',...
%     'PER_Last_mean','PER_Kalman_AR20_mean','PER_ML_mean','PER_Preamble_mean_Kalman_AR20',...
%     'PER_Preamble_mean_ML','PER_Preamble_Genie_mean','PER_LS_mean','CER_ML3_mean',...
%     'CER_ML1_mean','CER_Kalman_AR1_mean','CER_Kalman_AR5_mean','CER_NoEq_mean',...
%     'CER_Preamble_mean','CER_Last2_mean','CER_Last_mean','CER_Kalman_AR20_mean',...
%     'CER_ML_mean','CER_Preamble_Kalman_AR20_mean','CER_Preamble_ML_mean',...
%     'CER_Preamble_Genie_mean','CER_LS_mean');

load('Plot_Original_Data_Save.mat');

notch = 'off';
%% MSE Plots

figure;
boxplot([(MSE_ML3.'),(MSE_ML1.'),(MSE_ML.')],'Notch',notch,'Labels',{'VVD-100ms Future','VVD-33.3ms Future','VVD-Current'});%,'Whisker',10)
title(['MSE between the Versions' char(10) 'of Image Based Channel Estimation'])
grid on;
set(gca,'XTickLabelRotation',45,'FontSize',12)
ylabel('MSE')

figure;
boxplot([(MSE_AR1_taps.'),(MSE_AR5_taps.'),(MSE_AR20_taps.')],'Notch',notch,'Labels',{'Kalman AR(1)','Kalman AR(5)','Kalman AR(20)'});%,'Whisker',10)
title(['MSE between the Versions of' char(10) 'Kalman Filtering Based Channel Estimation'])
grid on;
set(gca,'XTickLabelRotation',45,'FontSize',12)
ylabel('MSE')

figure
boxplot([(MSE_Last2.'),(MSE_Last.'),(MSE_AR20_taps.'),(MSE_ML.'),(MSE_Preamble.'),(MSE_Preamble_Kalman_AR20.'),(MSE_Preamble_ML.'),(MSE_Preamble_Genie.')],'Notch',notch,'Labels',{'500ms Previous','100ms Previous','Kalman AR(20)','VVD-Current','Preamble Based','Preamble-Kalman Combined','Preamble-VVD Combined','Preamble Based-Genie'});%,'Whisker',10)
title(['MSE between Different Estimation' char(10) 'Techniques and the Ground Truth'])
grid on;
set(gca,'XTickLabelRotation',45,'yscale','log','FontSize',12)
% set(gca,'yscale','log')
ylim([2e-7 6e-6])
ylabel('MSE')
yticks([2e-7,1e-6])

%% PER Plots

figure
boxplot([(PER_ML3_mean.'),(PER_ML1_mean.'),(PER_ML_mean.')],'Notch',notch,'Labels',{'VVD-100ms Future','VVD-33.3ms Future','VVD-Current'});%,'Whisker',5)
title(['Packet Error Rate of Versions' char(10) 'of Image Based Channel Estimation'])
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([1e-3 1])
ylabel('PER')

figure
boxplot([(PER_Kalman_AR1_mean.'),(PER_Kalman_AR5_mean.'),(PER_Kalman_AR20_mean.')],'Notch',notch,'Labels',{'Kalman AR(1)','Kalman AR(5)','Kalman AR(20)'});%,'Whisker',5)
title(['Packet Error Rate of Versions' char(10) 'of Kalman Filtering Based Channel Estimation'])
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([1e-3 1])
ylabel('PER')

figure
boxplot([(PER_NoEq_mean.'),(PER_Preamble_mean.'),(PER_Last2_mean.'),(PER_Last_mean.'),(PER_Kalman_AR20_mean.'),(PER_ML_mean.'),(PER_Preamble_mean_Kalman_AR20.'),(PER_Preamble_mean_ML.'),(PER_Preamble_Genie_mean.'),(PER_LS_mean.')],'Notch',notch,'Labels',{'Standard Decoding','Preamble Based','500ms Previous','100ms Previous','Kalman AR(20)','ML-Current','Preamble-Kalman Combined','Preamble-ML Combined','Preamble Based-Genie','Ground Truth'});%,'Whisker',5)
title('Packet Error Rate of All Techniques')
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
ylim([2e-3 5e-1])
ylabel('PER')

%% CER Plots

figure
boxplot([(CER_ML3_mean.'),(CER_ML1_mean.'),(CER_ML_mean.')],'Notch',notch,'Labels',{'VVD-100ms Future','VVD-33.3ms Future','VVD-Current'});%,'Whisker',5)
title(['Chip Error Rate of Versions' char(10) 'of Image Based Channel Estimation'])
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([1e-3 1])
ylabel('CER')

figure
boxplot([(CER_Kalman_AR1_mean.'),(CER_Kalman_AR5_mean.'),(CER_Kalman_AR20_mean.')],'Notch',notch,'Labels',{'Kalman AR(1)','Kalman AR(5)','Kalman AR(20)'});%,'Whisker',5)
title(['Chip Error Rate of Versions' char(10) 'of Kalman Filtering Based Channel Estimation'])
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
% ylim([1e-3 1])
ylabel('CER')

figure
boxplot([(CER_NoEq_mean.'),(CER_Preamble_mean.'),(CER_Last2_mean.'),(CER_Last_mean.'),(CER_Kalman_AR20_mean.'),(CER_ML_mean.'),(CER_Preamble_Kalman_AR20_mean.'),(CER_Preamble_ML_mean.'),(CER_Preamble_Genie_mean.'),(CER_LS_mean.')],'Notch',notch,'Labels',{'Standard Decoding','Preamble Based','500ms Previous','100ms Previous','Kalman AR(20)','VVD-Current','Preamble-Kalman Combined','Preamble-VVD Combined','Preamble Based-Genie','Ground Truth'});%,'Whisker',5)
title('Chip Error Rate of All Techniques')
grid on;
set(gca,'yscale','log')
set(gca,'XTickLabelRotation',45,'FontSize',12)
ylim([9e-3 4e-1])
ylabel('CER')

%%