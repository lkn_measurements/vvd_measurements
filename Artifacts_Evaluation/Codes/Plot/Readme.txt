Here is the code for plotting the results. In this folder pre-obtained results are also provided in case directly to plot the same graphs in the paper.

'Plot_Fig5.m' plots the results that is the Figure 5 in the paper. It is the hypothesis testing part.

'Plot_Main_Results.m' This code plots the PER, MSE, and CER results separately for VVD and Kalman results as well as for the overall results

'Plot_Aging_TimevsPER.m' This code provides the aging plots in the paper as well as the plot for the 'Time versus decoding performance of different techniques' plot.

Provided 3 .png files are the colored frames of the dataset that are provided in the paper.