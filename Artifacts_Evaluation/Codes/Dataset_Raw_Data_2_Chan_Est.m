clear all;
close all;
clc;
%% Add measurement path and open the file
addpath('./Raw_Datasets/');
addpath('./Codes/');
addpath('./Codes/Helpers/');
%% Parameters of channel estimations
spc = 8;                                        % x samples per chip; the frame was captured at 8 x chiprate = 8 MHz
decimationFactor=1;
Rs = spc*1e6;

%% Load sets individually
for set_num = 1:15
    % Load raw signal
    % Use below code for automized calculation for different sets O.w.Loaded as i.e. 'Dataset_set8'
    raw_signal_file = strcat('./Raw_Datasets/Dataset_set',int2str(set_num),'.mat');
    raw_dataset = load(raw_signal_file);
    struct_str_dataset = fieldnames(raw_dataset);
    struct_str_dataset=struct_str_dataset{1};
    raw_dataset = raw_dataset.(struct_str_dataset);
    total_num_packets = size(raw_dataset,2);
    %% Frame number from the recorded images are stored after manual inspection for correctness
    %% Hence, they are called from the saved dataset.
    File_for_frames = strcat('./Obtained_Ground_Chan_Est_Datasets/Dataset_set',int2str(set_num),'_only_est_eq_phase.mat');
    Dataset_for_frames = load(File_for_frames);
    struct_str_dataset = fieldnames(Dataset_for_frames);
    struct_str_dataset=struct_str_dataset{1};
    Dataset_for_frames = Dataset_for_frames.(struct_str_dataset);
    %% Loop for reading the stored waveform
    for packet_count=1:total_num_packets
        
        waveform_packet = raw_dataset(packet_count).Raw_Packet_Signal;
        
        %% Coarse Frequency Compensation
        
%         % Normal version
%         coarseFrequencyCompensator = comm.CoarseFrequencyCompensator('Modulation',...
%             'OQPSK', 'SampleRate', spc*1e6/decimationFactor, 'FrequencyResolution', 1);
%         [coarseCompensatedOQPSK, coarseFrequencyOffset] = coarseFrequencyCompensator(waveform_packet);
%         
% %         % Below in case of CFO error (Downsampling reduced the interference effect on CFO Estimation)
% %         waveform_packet2 = waveform_packet(1:2:end);
% %         coarseFrequencyCompensator = comm.CoarseFrequencyCompensator('Modulation',...
% %             'OQPSK', 'SampleRate', spc*1e6/decimationFactor/2, 'FrequencyResolution', 1);
% %         [coarseCompensatedOQPSK, coarseFrequencyOffset] = coarseFrequencyCompensator(waveform_packet2);
%         
%         % Below to skip the signal if its frequency is not correctly estimated
%         if coarseFrequencyOffset>24000 || coarseFrequencyOffset<17000
%             Dataset_set(packet_count).Raw_Packet_Signal = waveform_packet;
% %             save(strcat('Chan_Est_Datasets/Dataset_set',int2str(set_num),'.mat'),'Dataset_set');
%             continue;
%         end
        %% CFO correction as it is already stored in the dataset
        coarseFrequencyOffset = raw_dataset(packet_count).CFO;
        freqoffset = comm.PhaseFrequencyOffset('SampleRate',spc*1e6/decimationFactor,'FrequencyOffsetSource','Input port');
        coarseCompensatedOQPSK = freqoffset(waveform_packet,-coarseFrequencyOffset);
        
        %% If needed to obtain MPDU from raw signal uncomment below
        %% Correct MPDU are already in the datasets
%         %% Timing Recovery
%         % Timing recovery of OQPSK signal, via its QPSK-equivalent version
%         
%         symbolSynchronizer = comm.SymbolSynchronizer('Modulation', 'OQPSK',...
%             'SamplesPerSymbol', spc/decimationFactor);
%         [syncedQPSK,~] = symbolSynchronizer(coarseCompensatedOQPSK);
%         
%         % Uncomment below to plot if any ambiguity occurs
%         % scatterplot(syncedQPSK);
%         % title('synched');
%         
%         %% Acquiring MPDU & its CRC check
%         % below the initialization for the loop is performed
%         dataFrameMACConfig = [];
%         k = 0;
%         
%         % This loop is to obtain the correct MPDU with 2 different ways
%         % Each case is visited only if the above one fails in Packet decoding or CRC
%         % check
%         while isempty(dataFrameMACConfig) && k<2
%             k = k+1;
%             switch (k)
%                 case 1
%                     [MPDU,phase_amb,preamble_start] = PHYDecoderOQPSKAfterSync_serkut_no_print(syncedQPSK);
%                     if ~isequal(MPDU,[])
%                         [dataFrameMACConfig] = lrwpan.MACFrameDecoder(MPDU);
%                     end
%                 case 2
%                     [MPDU,phase_amb,preamble_start] = PHYDecoderOQPSKNoSync_serkut_known_no_print(coarseCompensatedOQPSK,spc/decimationFactor);
%                     if ~isequal(MPDU,[])
%                         [dataFrameMACConfig] = lrwpan.MACFrameDecoder(MPDU);
%                     end
%             end
%         end
%         
%         % If above trials were failed to obtain MPDU or correct MPDU (CRC check failed)
%         % below code purpose is to avoid any estimation and iterates the loop for the next
%         % packet. This case gives an empty row with only starting and stop indexes
%         % on the Dataset.struct for the particular packet which indicates that packet
%         % within these indexes should be manually found and manually corrected !!
%         if size(MPDU,1)~=1016 || isempty(dataFrameMACConfig)
%             Dataset_set(packet_count).start_ind = start_idx_org;
%             Dataset_set(packet_count).stop_ind = stop_idx_org;
%             Dataset_set(packet_count).Raw_Packet_Signal = waveform_packet;
% %             save(strcat('Chan_Est_Datasets/Dataset_set',int2str(set_num),'.mat'),'Dataset_set');
%             continue;
%         end
        %% Obtain the ideal signal as at the transmitter end
        MPDU = raw_dataset(packet_count).MPDU;
        % Ideal signal with the correct bits in 802.15.4 standard before any effect of wireless channel
        original_waveform = lrwpan.PHYGeneratorOQPSK(MPDU, spc/decimationFactor, '2450 MHz'); 
        
        %% Find the starting point of the signal
        
        [acor,lag] = xcorr(coarseCompensatedOQPSK,original_waveform(1:end));
        [~,I]=max(abs(acor));
        Start_packet=I+lag(1);
        
        % Uncomment below to see plot of the cross-correlation result
        %         figure;
        %         plot(lag,abs(acor));
        % title('Cross Correlation of SHR and Synchronized Signal');
        %%
        for M = [size(original_waveform,1),1280] %number of reference symbols used for channel estimation. Former is for whole signal, latter is for the preamble estimation
                estimate_length=11;
%                 "estimate_length" chooses the length of the estimated filter (channel impulse response)
                A_conv=convmtx(original_waveform(1:M),estimate_length); % Convolution matrix
                estimation_start_offset = -(estimate_length+1)/2; % Observed to yield better results when the filter of the estimation is created symmetrical
                h_LS=((A_conv'*A_conv)\(A_conv'))*coarseCompensatedOQPSK(Start_packet+estimation_start_offset:size(A_conv,1)+Start_packet-1+estimation_start_offset); % LS solution
                
                %%
                % Uncomment below to plot estimations
                % figure(6);hold on;[H,f]=freqz([h_LS(:,1)],1,-Rs/2:Rs/200:Rs/2,Rs);grid on;
                % plot(f/1e6,20*log10(abs(H))); %legend('LS Channel Estimate');
                % xlabel('Frequency [MHz]');ylabel('Amplitude response [dB]');
                %%
                if M == size(original_waveform,1)
                    LS_11Tap = h_LS;
                else
                    Preamble_LS_11Tap = h_LS;
                end
                %% Mean Phase correction of all the estimations
                % After Getting all estimations, turn them into same phase to make them relatable for ML
                
                LOS_tap = round(estimate_length/2)+1;
                ang_ref = mean(angle(h_LS(LOS_tap)));
                mean_phase = pi/4-ang_ref;        % All estimation phases are referenced to pi/4
                corr_phase = (mean_phase/pi)*180; % Angles are turned to degrees from radian
                
                phaseoffset = comm.PhaseFrequencyOffset('SampleRate',spc*1e6/decimationFactor,...
                    'PhaseOffset',corr_phase);
                h_LS_corr = phaseoffset(h_LS); % Offset corrected versions
                %% FIR zero forcing equalizer (LS approach)

                if M == size(original_waveform,1)
                    LS_11Tap_Corr = h_LS_corr;
                else
                    Preamble_LS_11Tap_Corr = h_LS_corr;
                end
        end
        %% Channel Parameters savings for obtaining Dataset
        % Waveform is not stored to reduce dataset size when it is only
        % meant to be used for channel estimations
        Dataset_set(packet_count).Packet_Seq_Num = raw_dataset(packet_count).Packet_Seq_Num;
        Dataset_set(packet_count).CFO = coarseFrequencyOffset;
%         Dataset_set(packet_count).Raw_Packet_Signal = waveform_packet; 
%         Dataset_set(packet_count).MPDU = MPDU;

        Dataset_set(packet_count).LS_11Tap = LS_11Tap;
        
        Dataset_set(packet_count).LS_11Tap_Corr = LS_11Tap_Corr;

        % Below are the estimations obtained only using preamble
        Dataset_set(packet_count).Preamble_LS_11Tap = Preamble_LS_11Tap;
        
        Dataset_set(packet_count).Preamble_LS_11Tap_Corr = Preamble_LS_11Tap_Corr;
            
        % Frame numbers are put in this new dataset
        Dataset_set(packet_count).Frame_Number = Dataset_for_frames(packet_count).Frame_Number; 
        fprintf('Channel Estimation done for Packet: %d\n',packet_count);
    end

    %% Last Save
    save(strcat('./Chan_Est_Datasets/Dataset_set',int2str(set_num),'.mat'),'Dataset_set');
end
