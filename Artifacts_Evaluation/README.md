'Raw_Datasets' folder includes the dataset with raw IQ Samples for each received packet, along with the recorded CFO estimation during the measurement parsing, MPDU bits for each packet and the obtained channel estimations with LS method.

'Obtained_Ground_Chan_Est_Datasets' folder includes packets without the raw IQ samples to reduce dataset size for later usage. Additionally, this dataset includes image frame number for each packet in one measurement for later usage in Machine Learning.

'Chan_Est_Datasets' folder includes Kalman estimations and the VVD estimations that are obtained offline. These stored estimations are used later in the 'Main.m' code to obtain last comparison results. Inside there is a folder named as, 'LS_npy_versions', which includes the ground truth channel estimations in .npy versions that facilitates their usage in jupyter notebook. For more detail refer to provided .ipynb code.

'Frame_Number_Creation' folder explains how the frame numbers are added to the datasets. Since these are manually inspected it has to be done manually and the detailed explanations are given in .docx document inside this folder.

'Codes' folder includes all the codes in order to reobtain the results. 

'Images' folder includes all depth images in the dataset. Their usage should be understandable after observing 'VVD_Script.ipynb' in 'Codes' folder or 'npy_image_extraction.py' code.

More details can be found inside the respective folders.