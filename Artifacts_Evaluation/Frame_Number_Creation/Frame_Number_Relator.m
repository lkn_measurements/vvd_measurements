%% Packet transmission should be done in every 3 frames
%% However, there exist 2 frame drop due to camera-PC relation which are only in set 1 and set3
%% Also due to CSMA-CA there exist ambiguities which are reported in the .docx document in this folder
sets_frame_start = [420, 343, 219, 272, 350, 345, 345, 357, 342, 322, 297, ...
    286, 307, 372, 351];
sets_frame_change = [4296, 7297, 4239, 4703, 4538, 4722, 4881, 3993, 4008, 3301, 3897,...
    4012, 4120, 3492, 3105];
sets_frame_start2 = [4298, 0, 4241, 0, 0, 0, 0, 0, 0, 0, 0, ...
    0, 0, 0, 0];
sets_frame_stop = [8882, 0, 8042, 0, 0, 0, 0, 0, 0, 0, 0, ...
    0, 0, 0, 0];
for j=1:15
    k = 0;
    for i = sets_frame_start(j):3:sets_frame_change(j)
        k= k+1;
        set_Packet_Frames(j).Frame(k,1) = i+1;% 1 Frame Num diff wrt video and Frames
    end
    if sets_frame_start2(j)~=0
        for i = sets_frame_start2(j):3:sets_frame_stop(j)
            k = k+1;
            set_Packet_Frames(j).Frame(k,1) = i+1;% 1 Frame Num diff wrt video and Frames
        end
    end
end
Dataset_Packet_Frames = set_Packet_Frames; % Ideal case frame numbers only
% save('Dataset_sets_frame_numbers.mat','Dataset_Packet_Frames');
% packet_frame_count = 0;
% for p=1:15
%     packet_frame_count = packet_frame_count+size(set_Packet_Frames(p).Frame,1);
% end
%% Below is used manually to apply ambiguities!!! (Use with manual supervision only)
% %% Concatenate Frames to Datasets
% clearvars -except Dataset_Packet_Frames
% load Dataset_set15.mat
% manual_corr_indexes = find(cellfun(@isempty,{Dataset_set15.Packet_Seq_Num}));
% for p=1:size(Dataset_set15,2)
%     Dataset_set15(1,p).Frame_Number = Dataset_Packet_Frames(1,15).Frame(p);
% end
% 
% Dataset_set = Dataset_set15;
% 
% Dataset_set=rmfield(Dataset_set,'Raw_Packet_Signal');
% Dataset_set=rmfield(Dataset_set,'MPDU');


% %% Frame Corrections depending on noted ambiguitiess
% Dataset_set(916).Frame_Number
% Dataset_set(917).Frame_Number = Dataset_set(916).Frame_Number+5;
% Dataset_set(918).Frame_Number
% 
% save('Chan_Est_Datasets/Dataset_set15.mat','Dataset_set');
