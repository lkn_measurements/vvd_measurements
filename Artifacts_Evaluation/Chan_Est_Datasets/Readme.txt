'Kalman_Ests' folder includes the Kalman filter based blind channel estimations in the paper. The dataset here can be used directly in the 'Main.m' code.

'VVD_Ests' folder includes the VVD estimations with all the variants. These can be used in 'Main.m' code directly.

'LS_npy_Versions' folder includes the Ground Truth estimations in .npy versions. These can be reobtained by the 'VVD_Script.ipynb' code. These are created to facilitate estimation dataset usage in jupyter notebook for ML studies.


